## Описание

Каркас веб-приложения на Flask

Структура каталогов для проекта (в частности на Flask) https://habr.com/post/421887/

## Сборка

```
docker-compose build
```

## запуск

```
docker-compose up
```

## Утилиты

Добавление пользователя

```
./bin/api --json-rpc '{"jsonrpc": "2.0", "method": "user.add", "params": {"username": "admin", "password": "admin"}, "id": 1}'
```

Запуск из консоли

```
./run.py
```
