#!/usr/bin/env python3

__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import sys
import argparse
import traceback

from myapp import app as application


def main():
    parser = argparse.ArgumentParser(
        description='API',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser._optionals.title = "Необязательные аргументы"

    parser.add_argument(
        "--debug",
        default=True,
        action='store_true',
        help="Отладочная информация"
    )
    parser.add_argument("--port", default=8000, help="Порт")
    parser.add_argument("--config", default="devel", help="Файл конфигурации")

    args = parser.parse_args()

    application.config.from_object('config.{}'.format(args.config))

    application.run(debug=args.debug, host='0.0.0.0', port=args.port)


if __name__ == "__main__":
    try:
        main()
    except Exception as err:
        traceback.print_exc(file=sys.stdout)
        sys.exit(1)

    sys.exit(0)
