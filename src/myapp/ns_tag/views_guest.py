__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

from flask import abort, render_template

from .. import app, lib, models


def tags():
    """Список меток
    """
    pagedata = {}
    pagedata['title'] = 'Список меток - ' + app.config['TITLE']
    pagedata['tags'] = models.db_session.query(
        models.Tag
    ).all()
    body = render_template('guest/tags.html', pagedata=pagedata)
    return body


def tag_id(id: int, page: int = 1):
    """Метка
    """
    pagedata = {}
    tag = models.db_session.query(
        models.Tag
    ).filter(
        models.Tag.id == id
    ).first()
    if tag is None:
        abort(404)

    pagedata['title'] = 'Метка {} - {}'.format(
        tag.name,
        app.config['TITLE']
    )
    pagedata['tag'] = tag

    pages_idx = models.db_session.query(
        models.PageTag.page_id
    ).filter(
        models.PageTag.tag_id == id
    )
    pages = models.db_session.query(
        models.Page
    ).filter(
        models.Page.id.in_(pages_idx)
    ).order_by(
        models.Page.title.asc()
    )

    pagedata['pagination'] = lib.Pagination(
        page,
        app.config['ITEMS_ON_PAGE'],
        pages.count()
    )
    pagedata['pagination'].url = '/tag/{}/pages'.format(id)
    pagedata['pages'] = pages.all()

    body = render_template('guest/tag.html', pagedata=pagedata)
    return body
