__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

from flask import abort, render_template

from .. import app, models


def tags():
    """Список меток
    """
    pagedata = {}
    pagedata['title'] = 'Список меток - ' + app.config['TITLE']
    body = render_template('/private/skeleton.html', pagedata=pagedata)
    return body


def tag_id(id):
    """Метка
    """
    pagedata = {}
    tag = models.db_session.query(
        models.Tag
    ).filter(
        models.Tag.id == id
    ).first()
    if tag is None:
        abort(404)

    pagedata['title'] = 'Метка {} - {}'.format(
        tag.name,
        app.config['TITLE']
    )

    body = render_template('/private/skeleton.html', pagedata=pagedata)
    return body


def tag_notes(id):
    """Метка
    """
    pagedata = {}
    tag = models.db_session.query(
        models.Tag
    ).filter(
        models.Tag.id == id
    ).first()
    if tag is None:
        abort(404)

    pagedata['title'] = 'Метка {} - {}'.format(
        tag.name,
        app.config['TITLE']
    )

    body = render_template('/private/skeleton.html', pagedata=pagedata)
    return body
