__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

from flask import abort

from . import views_user, views_guest
from .. import app, lib


@app.route('/tags')
def tags():
    """Список меток
    """
    if lib.get_user():
        return views_user.tags()
    else:
        return views_guest.tags()


@app.route('/tag/<int:id>')
def tag_id(id):
    """Метка
    """
    if lib.get_user():
        return views_user.tag_id(id)
    else:
        return views_guest.tag_id(id)


@app.route('/tag/<int:id>/notes')
def tag_notes(id):
    """Заметки с меткой
    """
    if lib.get_user():
        return views_user.tag_notes(id)
    else:
        abort(404)
