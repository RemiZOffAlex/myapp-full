__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

from flask import render_template, session, redirect

from .. import app


@app.route('/login')
def login():
    """Вход
    """
    pagedata = {}
    body = render_template('login.html', pagedata=pagedata)
    return body


@app.route('/logout')
def logout():
    """Выход
    """
    session.pop('logged_in', None)
    session.pop('user_id', None)
    return redirect("/", code=302)


@app.route('/register')
def register():
    """Регистрация нового пользователя
    """
    pagedata = {}
    body = render_template('register.html', pagedata=pagedata)
    return body
