__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import config
import logging

from flask import Flask
from logging.handlers import RotatingFileHandler


app = Flask(__name__)
app.config.from_object(config.CONFIG)

app.config.from_envvar('FLASKR_SETTINGS', silent=True)

# Логирование
handler = RotatingFileHandler(
    app.config['LOG_FILE'],
    maxBytes=app.config['LOG_FILE_SIZE']*1024*1024,
    backupCount=1
)
handler.setLevel(logging.INFO)
formatter = logging.Formatter(app.config['LONG_LOG_FORMAT'])
handler.setFormatter(formatter)
app.logger.addHandler(handler)


from . import lib, models


@app.context_processor
def inject_data():
    result = {}

    result['STATIC'] = app.config['STATIC']
    result['ITEMS_ON_PAGE'] = app.config['ITEMS_ON_PAGE']

    result['user'] = None
    if lib.get_user():
        result['user'] = lib.get_user().as_dict()

    return result


@app.teardown_appcontext
def shutdown_session(exception=None):
    models.db_session.close_all()

# API
from . import ns_api # noqa F401

# Общее
from . import ns_common # noqa F401

# Авторизация
from . import ns_login # noqa F401

# Заметки
from . import ns_note # noqa F401

# Статьи
from . import ns_page # noqa F401

# Профиль
from . import ns_profile # noqa F401

# Метки
from . import ns_tag # noqa F401

# Пользователи
from . import ns_user # noqa F401

from . import views # noqa F401
