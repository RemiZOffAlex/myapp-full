__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import os
import jinja2

from . import views # noqa F401
from .. import app


my_loader = jinja2.ChoiceLoader([
    app.jinja_loader,
    jinja2.FileSystemLoader(
        os.path.dirname(os.path.abspath(__file__)) + "/templates"
    ),
])
app.jinja_loader = my_loader
