__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

from flask import render_template

from .. import app
from ..decorators import login_required


@app.route('/profile')
@login_required
def profile():
    """
    Личный профиль пользователя
    """
    pagedata = {}
    pagedata['title'] = 'Мой профиль - {}'.format(app.config['TITLE'])
    body = render_template('profile.html', pagedata=pagedata)
    return body


@app.route('/profile/pages')
@login_required
def profile_pages():
    """
    Личный профиль пользователя
    """
    pagedata = {}
    pagedata['title'] = 'Мой профиль - {}'.format(app.config['TITLE'])

    pagedata['pagination'] = {
        "page": 1,
        "per_page": app.config['ITEMS_ON_PAGE'],
        "size": 0
    }

    body = render_template('profile_pages.html', pagedata=pagedata)
    return body
