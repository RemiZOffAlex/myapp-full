#!/usr/bin/env python3

__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import datetime

from functools import wraps
from flask import session, redirect

from . import models


def login_required(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if 'logged_in' in session and 'user_id' in session:
            user = models.db_session.query(
                models.User
            ).filter(
                models.User.id == session['user_id']
            ).first()
            if user:
                # user.last_activity = datetime.datetime.now()
                # models.db_session.commit()
                return func(*args, **kwargs)
            else:
                session.pop('logged_in', None)
                session.pop('user_id', None)
                return redirect('/login')
        else:
            return redirect('/login')
    return decorated_function
