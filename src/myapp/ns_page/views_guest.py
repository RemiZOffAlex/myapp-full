__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

from flask import abort, render_template

from .. import app, models


def pages(page):
    """Список статей
    """
    pagedata = {'title': 'Статьи - ' + app.config['TITLE']}

    pages = models.db_session.query(
        models.Page
    ).all()
    pagedata['pages'] = pages

    pagedata['pagination'] = {
        "page": page,
        "per_page": app.config['ITEMS_ON_PAGE'],
        "size": 0
    }

    body = render_template('guest/pages.html', pagedata=pagedata)
    return body


def page_id(id):
    """Документ
    """
    page = models.db_session.query(
        models.Page
    ).filter(
        models.Page.id == id
    ).first()
    if page is None:
        abort(404)

    pagedata = {}
    pagedata['title'] = '{} - {}'.format(
        page.title,
        app.config['TITLE']
    )

    pagedata['page'] = page

    body = render_template('guest/page.html', pagedata=pagedata)
    return body
