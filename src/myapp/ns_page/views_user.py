__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

from flask import abort, render_template

from .. import app, models


def pages(page):
    """Список статей
    """
    pagedata = {'title': 'Статьи - ' + app.config['TITLE']}

    pagedata['pagination'] = {
        "page": page,
        "per_page": app.config['ITEMS_ON_PAGE'],
        "size": 0
    }

    body = render_template('user/pages.html', pagedata=pagedata)
    return body


def page_id(id):
    """Статья
    """
    page = models.db_session.query(
        models.Page
    ).filter(
        models.Page.id == id
    ).first()
    if page is None:
        abort(404)

    pagedata = {}
    pagedata['title'] = '{} - {}'.format(page.title, app.config['TITLE'])

    pagedata['page'] = page.as_dict()
    pagedata['page']['user'] = page.user.as_dict()

    # Теги
    pagedata['page']['tags'] = []
    for tagLink in page.tags:
        pagedata['page']['tags'].append(tagLink.tag.as_dict())

    pagedata['title'] = '{} - {}'.format(
        page.title,
        app.config['TITLE']
    )
    body = render_template('user/page.html', pagedata=pagedata)
    return body


def page_edit(id):
    """Редактирование статьи
    """
    pagedata = {'title': app.config['TITLE']}
    page = models.db_session.query(
        models.Page
    ).filter(
        models.Page.id == id
    ).first()
    if page is None:
        abort(404)
    pagedata['title'] = '{} - {}'.format(page.title, pagedata['title'])
    pagedata['page'] = page.as_dict()
    body = render_template('user/page_edit.html', pagedata=pagedata)
    return body


def page_add():
    """Добавление нового документа
    """
    pagedata = {}
    pagedata['title'] = 'Новый документ - ' + app.config['TITLE']
    body = render_template('/private/skeleton.html', pagedata=pagedata)
    return body
