__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

from flask import abort

from . import views_user, views_guest
from .. import app, lib


@app.route('/page/<int:id>')
def page_id(id):
    """Статья"""
    if lib.get_user():
        return views_user.page_id(id)
    else:
        return views_guest.page_id(id)


@app.route('/page/<int:id>/edit')
def page_edit(id):
    """Редактирование статьи"""
    if lib.get_user():
        return views_user.page_edit(id)
    else:
        abort(404)


@app.route('/page/add')
def page_add():
    """Добавление новой статьи
    """
    if lib.get_user():
        return views_user.page_add()
    else:
        abort(404)


@app.route('/pages', defaults={'page': 1})
@app.route('/pages/<int:page>')
def pages(page):
    """Список статей"""
    if lib.get_user():
        return views_user.pages(page)
    else:
        return views_guest.pages(page)
