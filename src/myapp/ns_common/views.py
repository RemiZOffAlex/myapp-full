__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

from flask import render_template

from .. import app, lib


@app.route('/')
def index():
    pagedata = {}
    pagedata['title'] = app.config['TITLE']
    if lib.get_user():
        pagedata['info'] = 'Закрытый раздел'
        body = render_template('user/index.html', pagedata=pagedata)
    else:
        pagedata['info'] = 'Открытый раздел'
        body = render_template('guest/index.html', pagedata=pagedata)
    return body
