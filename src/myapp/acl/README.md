Удаление неиспользуемых ACL

В файле specialistoff/acl/acl.py

```
# import json
# from pathlib import Path

...

        # data = {}
        # acl_file = Path(app.config['DIR_DATA'] + "/acl.json")
        # if acl_file.exists():
        #     with open(acl_file, 'r') as fd:
        #         data = json.load(fd)
        # if view_func.__module__ not in data:
        #     data[view_func.__module__] = []
        # if view_func.__name__ not in data[view_func.__module__]:
        #     data[view_func.__module__].append(view_func.__name__)
        # with open(acl_file, 'w') as fd:
        #     json.dump(data, fd)
```

```
import json
from pathlib import Path

from specialistoff import app, lib, models

data = {}
acl_file = Path(app.config['DIR_DATA'] + "/acl.json")
if acl_file.exists():
    with open(acl_file, 'r') as fd:
        data = json.load(fd)


for item in data:
    resources = models.db_session.query(
        models.ACLResource
    ).filter(
        models.ACLResource.modulename == item
    ).filter(
        ~models.ACLResource.funcname.in_(data[item])
    )
    if resources.count() > 0:
        print(resources.all())
```
