__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

from flask import abort, render_template

from .. import app, lib, models


def user_id(id):
    """Пользователь
    """
    pagedata = {}
    user = models.db_session.query(
        models.User
    ).filter(
        models.User.id == id
    ).first()
    if user is None:
        abort(404)
    pagedata['user'] = user
    pagedata['title'] = '{} - {}'.format(user.name, app.config['TITLE'])
    body = render_template('/guest/user.html', pagedata=pagedata)
    return body


def user_pages(id: int, page: int = 1):
    """Список статей пользователя
    """
    user = models.db_session.query(
        models.User
    ).filter(
        models.User.id == id
    ).first()
    if user is None:
        abort(404)
    pagedata = {}
    pagedata['user'] = user.as_dict()
    pagedata['title'] = '{} - {}'.format(user.name, app.config['TITLE'])

    pages = models.db_session.query(
        models.Page
    ).filter(
        models.Page.user_id == id
    ).order_by(
        models.Page.title.asc()
    )

    pagedata['pagination'] = lib.Pagination(
        page,
        app.config['ITEMS_ON_PAGE'],
        pages.count()
    )
    pagedata['pagination'].url = '/user/{}/pages'.format(user_id)
    pagedata['pages'] = pages.all()

    body = render_template('/guest/user_pages.html', pagedata=pagedata)
    return body


def users_list(page: int = 1):
    """Список пользователей
    """
    pagedata = {}
    pagedata['title'] = 'Список пользователей - ' + app.config['TITLE']

    users = models.db_session.query(
        models.User
    ).order_by(
        models.User.name.asc()
    )

    pagedata['pagination'] = lib.Pagination(
        page,
        app.config['ITEMS_ON_PAGE'],
        users.count()
    )
    pagedata['pagination'].url = '/users'
    pagedata['users'] = users.all()

    body = render_template('/guest/users.html', pagedata=pagedata)
    return body
