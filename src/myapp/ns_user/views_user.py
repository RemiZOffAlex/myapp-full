__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

from flask import abort, render_template

from .. import app, models


def user_id(id):
    """Пользователь
    """
    pagedata = {}
    user = models.db_session.query(
        models.User
    ).filter(
        models.User.id == id
    ).first()
    if user is None:
        abort(404)
    pagedata['user'] = user.as_dict()
    pagedata['title'] = '{} - {}'.format(user.name, app.config['TITLE'])
    body = render_template('user.html', pagedata=pagedata)
    return body


def user_pages(id):
    """Список статей пользователя
    """
    pagedata = {}
    user = models.db_session.query(
        models.User
    ).filter(
        models.User.id == id
    ).first()
    if user is None:
        abort(404)
    pagedata['user'] = user.as_dict()
    pagedata['title'] = '{} - {}'.format(user.name, app.config['TITLE'])

    pagedata['pagination'] = {
        "page": 1,
        "per_page": app.config['ITEMS_ON_PAGE'],
        "size": 0
    }

    body = render_template('user_pages.html', pagedata=pagedata)
    return body


def users_list(page: int):
    """Список пользователей
    """
    pagedata = {}
    pagedata['title'] = 'Список пользователей - ' + app.config['TITLE']

    pagedata['pagination'] = {
        "page": page,
        "per_page": app.config['ITEMS_ON_PAGE'],
        "size": 0
    }

    body = render_template('users.html', pagedata=pagedata)
    return body
