__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

from flask import abort

from . import views_user, views_guest
from .. import app, lib


@app.route('/user/<int:id>')
def user_id(id):
    """Пользователь
    """
    if lib.get_user():
        return views_user.user_id(id)
    else:
        return views_guest.user_id(id)


@app.route('/user/<int:id>/pages', defaults={'page': 1})
@app.route('/user/<int:id>/pages/<int:page>')
def user_pages(id: int, page: int):
    """Список статей пользователя
    """
    if lib.get_user():
        return views_user.user_pages(id, page)
    else:
        return views_guest.user_pages(id, page)
        abort(404)


@app.route('/users', defaults={'page': 1})
@app.route('/users/<int:page>')
def users_list(page: int):
    """
    Список пользователей
    """
    if lib.get_user():
        return views_user.users_list(page)
    else:
        return views_guest.users_list(page)
        abort(404)
