/*
<pagination-component v-bind:pagination="pagination" v-bind:click-handler="getRecipes"></pagination-component>
<script type="text/javascript" src="/static/components/pagination.js"></script>

Vue
...
data: {
    pagination: {
        page: 1,
        per_page: {{ per_page }},
        size: {{ pagedata['count'] }}
    },
}
...

Flask
    pagedata['pagination'] = {
        "page": page,
        "per_page": app.config['ITEMS_ON_PAGE'],
        "size": 0
    }

*/

let paginationTemplate = `<div class="row">
<div class="col py-2 text-center">
<div class="btn btn-outline-secondary" v-if="pages()<=1">...</div>

<template v-else>
<button type="button" class="btn btn-outline-secondary pull-left" v-if="has_prev" v-on:click="handlePageSelected(pagination.page-1)">Предыдущая</button>

<template v-for="page in iter_pages">
<button type="button" class="btn btn-outline-secondary mr-1" v-if="page" v-on:click="handlePageSelected(page)">{{ page }}</button>
<div class="btn btn-outline-secondary mr-1" v-else>...</div>
</template>

<button type="button" class="btn btn-outline-secondary float-right" v-if="has_next" v-on:click="handlePageSelected(pagination.page+1)">Следующая</button>
</template>

</div>
</div>`;

root.components['pagination-component'] = {
    template: paginationTemplate,
    data: function() {
        return {
        }
    },
    props:{
        pagination: {
            type: Array,
            required: true,
            default: {
                page: 1,
                per_page: 25,
                size: 0
            }
        },
        clickHandler: {
            type: Function,
            default: function() { }
        },
    },
    methods: {
        handlePageSelected: function(selected) {
            /* Установить номер текущей страницы и вызвать функция обновления
             * страницы
             * 
             * Аргументы:
             * selected -- номер страницы
             */
            let vm = this;
            vm.pagination.page = selected;
            vm.clickHandler();
        },
        pages: function() {
            let vm = this;
            return Math.ceil(vm.pagination.size/vm.pagination.per_page);
        },
    },
    computed: {
        has_prev: function() {
            let vm = this;
            return vm.pagination.page > 1;
        },
        has_next: function() {
            let vm = this;
            return vm.pagination.page < vm.pages();
        },
        iter_pages: function() {
            /* */
            let vm = this;
            let last = 0;
            let left_edge=2, left_current=2,
                   right_current=5, right_edge=2;
            let result = [];
            for (let num = 1; num < vm.pages()+1; num++) {
                if (num <= left_edge ||
                    (num > vm.pagination.page - left_current - 1 &&
                    num < vm.pagination.page + right_current) ||
                    num > vm.pages() - right_edge) {
                    if (last + 1 != num) {
                        result.push(null);
                    } else {
                        result.push(num);
                    }
                    last = num
                }
            };
            return result;
        },
    }
};
