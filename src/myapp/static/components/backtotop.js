var backtotopTemplate = `
<div>
<span class="scrollToTop" style="z-index: 10;" v-show="visible" v-on:click="backToTop">
<div class="card">
<div class="card-body py-2 px-2">
<i class="fa fa-chevron-up"></i>
</div>
</div>
</span>
</div>`;

root.components['backtotop-component'] = {
    props: {
        visibleoffset: {
            type: [String, Number],
            default: 600
        },
    },
    template: backtotopTemplate,
    data () {
        return {
            visible: false
        }
    },
    mounted () {
        let vm = this;
        window.addEventListener('scroll', this.catchScroll);
        let currentScroll = document.documentElement.scrollTop || document.body.scrollTop
        vm.visible = (currentScroll > 100);
    },
    destroyed () {
        window.removeEventListener('scroll', this.catchScroll)
    },
    methods: {
        catchScroll () {
            let vm = this;
            vm.visible = (window.pageYOffset > 100);
        },
        backToTop () {
            let vm = this;
            vm.scrollAnimate();
        },
        scrollAnimate: function() {
            let vm = this;
            let currentScroll = document.documentElement.scrollTop || document.body.scrollTop
            if (currentScroll > 0) {
                //alert(currentScroll);
                window.requestAnimationFrame(vm.scrollAnimate)
                window.scrollTo(0, Math.floor(currentScroll - (currentScroll / 5)))
            }
        }
    }
};
