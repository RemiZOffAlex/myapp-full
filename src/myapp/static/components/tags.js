/*
<tags-component v-bind:tags="document.tags" v-bind:resource="{id: document.id, name: 'document'}"></tags-component>
<script type="text/javascript" src="/static/components/tags.js"></script>

*/

var tagsTemplate = `
<div>
<!-- Начало: Теги -->
<div class="row my-3">
<div class="col">
<div class="btn mb-1"><i class="fa fa-tags"></i></div>

<div class="btn btn-outline-success mb-1" v-on:click="panel_show(panels.standart)"><i class="fa fa-plus"></i></div>

<div class="btn-group mr-2 mb-1" v-for="(tag, tagIdx) in sortedTags">
<a class="btn btn-outline-secondary text-monospace" :href="'/tag/' + tag.id">{{ tag.name }}</a>
<div class="btn btn-outline-danger" v-on:click="removeTag(tag)"><i class="fa fa-remove"></i></div>
</div>

</div>
</div>

<div class="row mt-3" v-if="panels.standart.visible">
<div class="col">

<div class="input-group">
<input v-model="newtag" v-on:keyup.13="addTag" class="form-control" />
<div class="input-group-append">
<div class="btn btn-outline-success" v-on:click="addTag"><i class="fa fa-save"></i></div>
</div>
</div>

<div class="row">
<div class="col py-2">
<template v-for="(tags, key, index) in groups">

<a class="btn btn-outline-secondary mt-2 mr-2" v-if="(index % 2)===0" :href="'#' + index">{{ key }}</a>
<a class="btn btn-outline-primary mt-2 mr-2" v-else :href="'#' + index">{{ key }}</a>

</template>
</div>
</div>

<!-- Начало: Форма добавления нового тега -->
<div class="row" v-if="panels.new.visible">
<div class="col py-2">
Добавить новый тег [<b id="newTag">{{ newtag }}</b>]?

<div class="row">
<div class="col py-2">
    <div class="btn btn-outline-danger" v-on:click="cancelNewTag">Отмена</div>
    <div class="btn btn-outline-success float-right" v-on:click="addNewTag">Добавить</div>
</div>
</div>

</div>
</div>
<!-- Конец: Форма добавления нового тега -->

<div class="row mt-3" v-for="(tags, key, index) in groups">
<div class="col pr-0">
<a :name="index" class="btn btn-outline-danger mr-2 mb-1">{{ key }}</a>
<template v-for="(tag, tagIdx) in tags">
<button type="button" class="btn btn-primary mr-2 mb-1" v-if="tag_includes(tag)" v-on:click="removeTag(tag)">{{ tag.name }}</button>
<button type="button" class="btn btn-outline-secondary mr-2 mb-1" v-else v-on:click="tag_add_to_node(tag)">{{ tag.name }}</button>
</template>
</div>
</div>

</div>
</div>
<!-- Конец: Теги -->
</div>`;

root.components['tags-component'] = {
    data: function() {
        return {
            newtag: '',
            groups: {},
            panels: {
                standart: {
                    visible: false
                },
                new: {
                    visible: false
                }
            }
        }
    },
    props: ['tags', 'resource'],
    template: tagsTemplate,
    methods: {
        arrayRemove: function(arr, value) {
            /* Удаление элемента из списка */
            return arr.filter(function(ele){
                return ele != value;
            });
        },
        removeTag: function (tag) {
            /* Удаление тега из ресурса */
            let vm = this;
            axios.post(
                '/api',
                {
                    "jsonrpc": "2.0",
                    "method": 'tag.' + vm.resource.name + '.delete',
                    "params": {
                        "tag": tag.id,
                        "id": vm.resource.id
                    },
                    "id": 1
                }
            ).then(
                function(response) {
                    if ('result' in response.data) {
                        vm.tags = vm.arrayRemove(vm.tags, vm.tag_includes(tag));
                    } else if ('error' in response.data) {
                        console.log(response.data);
                    }
                }
            );
        },
        addTag: function () {
            /* Добавить тег к ресурсу */
            let vm = this;
            var newtag = vm.newtag.trim();
            vm.newtag = newtag;
            axios.post(
                '/api',
                {
                    "jsonrpc": "2.0",
                    "method": 'tag.exist',
                    "params": {
                        name: vm.newtag
                    },
                    "id": 1
                }
            ).then(
                function(response) {
                    if ('result' in response.data) {
                        vm.tag_add_to_node( response.data['result'] );
                    } else if ('error' in response.data) {
                        console.log(response.data);
                        vm.panels.new.visible = true;
                    }
                }
            ).catch(
                function (error) {
                    console.log(error);
                }
            );
        },
        addNewTag:function () {
            /* Добавление нового тега */
            let vm = this;
            axios.post(
                '/api',
                {
                    "jsonrpc": "2.0",
                    "method": 'tag.add',
                    "params": {
                        name: vm.newtag
                    },
                    "id": 1
                }
            ).then(
                function(response) {
                    if ('result' in response.data) {
                        vm.getTags();
                        vm.tag_add_to_node( response.data['result'] );
                    }
                }
            );
        },
        cancelNewTag:function () {
            let vm = this;
            vm.newtag = '';
            vm.panels.new.visible = false;
        },
        // Добавление тега к ресурсу
        tag_add_to_node: function(tag) {
            let vm = this;
            axios.post(
                '/api',
                {
                    "jsonrpc": "2.0",
                    "method": 'tag.' + vm.resource.name + '.add',
                    "params": {
                        "id": vm.resource.id,
                        "tag": tag.id
                    },
                    "id": 1
                }
            ).then(
                function(response) {
                    if ('result' in response.data) {
                        vm.tags.push(response.data['result']);
                        vm.newtag = '';
                        vm.panels.new.visible = false;
                    }
                }
            );
        },
        // Получить список тегов
        getTags: function() {
            let vm = this;
            axios.post(
                '/api',
                {
                    "jsonrpc": "2.0",
                    "method": 'tags.groups',
                    "params": {},
                    "id": 1
                }
            ).then(
                function(response) {
                    if ('result' in response.data) {
                        vm.groups = response.data['result'];
                    }
                }
            ).catch(
                function (error) {
                    console.log(error);
                }
            );
        },
        panel_show: function(panel) {
            /* Показать/скрыть панель */
            panel.visible = !panel.visible;
        },
        tag_includes: function(tag) {
            let vm = this;
            let result = vm.tags.find(function(element, index, array) {
                return element.id===tag.id;
            }, tag);
            return result;
        },
    },
    mounted: function() {
        let vm = this;
        vm.getTags();
    },
    computed: {
        sortedTags: function() {
            let vm = this;
            if (vm.tags === undefined) {return [];}
            vm.tags.sort(
                function(a, b) {
                    if (a.name > b.name) {
                        return 1;
                    }
                    if (a.name < b.name) {
                        return -1;
                    }
                    return 0;
                }
            );
            return vm.tags;
        }
    }
};
