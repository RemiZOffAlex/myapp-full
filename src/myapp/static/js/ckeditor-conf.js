CKEDITOR.editorConfig = function( config ) {
    config.language = 'ru';
    config.fillEmptyBlocks = false;
    config.tabSpaces = 4;
    config.tabIndex = 4;
    config.allowedContent = true;
    config.stylesSet = [
        { name: 'Цитата', element: 'blockquote', attributes: { 'class': 'blockquote'} },
        { name: 'Картинка', element: 'img', attributes: { 'class': 'img-thumbnail', 'style': ''} },
        { name: 'Блок code', element: 'code'},
        { name: 'Блок pre', element: 'pre'},
        { name: 'Клавиша', element: 'kbd'},
        { name: 'Махонький текст', element: 'small'},
        { name: 'Примечание', element: 'p', attributes: { 'class': 'alert alert-primary'} },
        { name: 'Предупреждение', element: 'p', attributes: { 'class': 'alert alert-warning'} },
        { name: 'Внимание', element: 'p', attributes: { 'class': 'alert alert-danger'} }
    ];
    config.toolbarGroups = [
        { name: 'tools', groups: [ 'tools' ] },
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        { name: 'forms', groups: [ 'forms' ] },
        { name: 'insert', groups: [ 'insert' ] },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        { name: 'styles', groups: [ 'styles' ] },
        '/',
        { name: 'colors', groups: [ 'colors' ] },
        { name: 'others', groups: [ 'others' ] },
        { name: 'about', groups: [ 'about' ] }
    ];

    config.removeButtons = 'Save,NewPage,Templates,PasteFromWord,Find,Replace,SelectAll,Scayt,Form,Radio,Checkbox,TextField,Textarea,Select,Button,ImageButton,HiddenField,Subscript,Superscript,CopyFormatting,CreateDiv,BidiLtr,BidiRtl,Language,Flash,About,ShowBlocks,BGColor,TextColor,Font,FontSize,Iframe,PageBreak,Smiley,JustifyLeft,JustifyCenter,JustifyRight,JustifyBlock,Preview,Print';
}
CKEDITOR.dtd.$removeEmpty['i'] = false;
