__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

from flask import session
from .. import models


def get_user():
    """Получить текущего пользователя
    """
    result = None
    if 'user_id' in session:
        result = models.db_session.query(
            models.User
        ).filter(
            models.User.id == session['user_id']
        ).first()
        if result is None:
            session.pop('logged_in', None)
            session.pop('user_id', None)
            return None
    return result
