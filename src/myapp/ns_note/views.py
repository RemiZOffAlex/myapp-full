__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

from flask import abort, render_template

from .. import app, models


@app.route('/note/<int:id>')
def note_id(id):
    """Заметка"""
    note = models.db_session.query(
        models.Note
    ).filter(
        models.Note.id == id
    ).first()
    if note is None:
        abort(404)

    pagedata = {}
    pagedata['title'] = '{} - {}'.format(note.title, app.config['TITLE'])

    pagedata['note'] = note.as_dict()
    pagedata['note']['user'] = note.user.as_dict()
    pagedata['note']['tags'] = []
    for tagLink in note.tags:
        pagedata['note']['tags'].append(tagLink.tag.as_dict())

    body = render_template('note.html', pagedata=pagedata)
    return body


@app.route('/note/add')
def note_add():
    """Добавление новой заметки
    """
    pagedata = {}
    pagedata['title'] = 'Новая заметка - ' + app.config['TITLE']
    body = render_template('note_add.html', pagedata=pagedata)
    return body


@app.route('/notes', defaults={'page': 1})
@app.route('/notes/<int:page>')
def notes_list(page):
    """Список заметок
    """
    pagedata = {}

    pagedata['pagination'] = {
        "page": page,
        "per_page": app.config['ITEMS_ON_PAGE'],
        "size": 0
    }

    body = render_template('notes.html', pagedata=pagedata)
    return body
