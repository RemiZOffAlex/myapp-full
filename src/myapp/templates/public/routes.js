m.route.prefix = '';
function layout_decorator(controller) {
    return {
        render: function(vnode) {
            return m(Layout, m(controller, vnode.attrs))
        }
    }
};

m.route(
    vroot,
    "/",
    routes
);
