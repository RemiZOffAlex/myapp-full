function layout_decorator(controller) {
    return {
        render: function(vnode) {
            return m(Layout, m(controller, vnode.attrs))
        }
    }
};
function Layout() {
    return {
        view: function(vnode) {
            let result = [
                m(MenuGeneral),
                m("section", vnode.children),
                m(Footer),
                m(Backtotop),
            ];
            return result;
        }
    }
};
