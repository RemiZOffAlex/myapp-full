function Tags() {
    let data = {
        filter: PanelFilter(),
        raw_groups: {},
        get groups() {
            let result = {};
            Object.keys(data.raw_groups).forEach(
                function (group, groupIdx) {
                    let tags = data.raw_groups[group].filter(tag_filter);
                    if (tags.length > 0) {
                        result[group] = tags;
                    }
                }
            );
            return result;
        },
    };
    function breadcrumbs_render() {
        let result = m('ul', { class: 'breadcrumb mt-2' }, [
            m('li', { class: 'breadcrumb-item' }, m(m.route.Link, { href: '/' }, m('i', { class: 'fa fa-home' }))),
            m('li', { class: 'breadcrumb-item active' }, 'Список тегов'),
        ]);
        return result;
    };
    function tag_filter(tag) {
        let filter = data.filter.data;
        let value = filter.value;
        if (value.length < 1) {
            return true;
        }
        if (tag.name.toLowerCase().includes(value.toLowerCase())) {
            return true;
        }
        return false;
    };
    function tags_get() {
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": 'tags.groups',
                "params": {
                },
                "id": get_id()
            }
        }).then(
            function (response) {
                if ('result' in response) {
                    data.raw_groups = response['result'];
                }
            }
        );
    };
    return {
        oninit: function (vnode) {
            console.log('Tags.oninit');
            tags_get();
        },
        view: function (vnode) {
            console.log('Tags.view');
            result = [];
            result.push(
                breadcrumbs_render(),
                m('div', { class: 'row' },
                    m('div', { class: "col h1 py-2" }, [
                        m('button', { type: "button", class: "btn btn-outline-secondary btn-lg me-2", onclick: function () { panel_show(data.filter.data) } },
                            m('i', { class: "fa fa-filter" })
                        ),
                        'Облако тегов'
                    ])
                ),
                m('hr'),
            );
            result.push(m(data.filter));
            if (Object.keys(data.groups).length > 0) {
                let groups = [];
                Object.keys(data.groups).forEach(
                    function (group, groupIdx) {
                        let odd = '';
                        if (groupIdx % 2) {
                            odd = 'btn-primary'
                        };
                        groups.push({ tag: '<', children: `<a href="#${groupIdx}" class="btn ${odd} btn-lg my-1 mx-1">${group}</a>` });
                    }
                );
                result.push(
                    m('div', { class: 'row' },
                        m('div', { class: "col text-justify" }, [...groups])
                    )
                )
                groups = [];
                Object.keys(data.groups).forEach(
                    function (group, groupIdx) {
                        let odd = '';
                        if (groupIdx % 2) {
                            odd = ' bg-light'
                        };
                        let tags = data.groups[group].map(
                            function (tag, tagIdx) {
                                return m(m.route.Link, { class: "btn btn-outline-secondary font-monospace my-1 me-2", href: `/tag/${tag.id}` }, tag.name);
                            }
                        )
                        groups.push(m('div', { class: 'row' }, [
                            m('div', { class: "col-md-1 py-2" + odd }, [
                                { tag: '<', children: `<a id=${groupIdx} class="btn btn-outline-danger w-100 my-1">${group}</a>` },
                                // m(m.route.Link, {class: "text-decoration-none", href: "/tag/" + tag.id, title: "Тег #" + tag.id}, tag.name),
                            ]),
                            m('div', { class: "col-md-11 py-2" + odd }, [...tags]),
                        ]))
                    }
                );
                result.push(...groups);
            };
            result.push(breadcrumbs_render());
            return result;
        }
    }
};
