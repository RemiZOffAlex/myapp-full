function TagPages() {
    let data = {
        filter: PanelFilter(),
        order_by: PanelOrderBy({
            field: 'title',
            fields: [
                {value: 'id', text: 'ID'},
                {value: 'title', text: 'заголовку'},
                {value: 'created', text: 'дате создания'},
                {value: 'updated', text: 'дате обновления'}
            ],
            clickHandler: pages_get,
            order: 'asc',
        }),
        tag: null,
        raw_pages: [],
        get pages() {
            let result = data.raw_pages.filter(page_filter);
            return result;
        },
        pagination: {
            page: 1,
            size: 0,
            prefix_url: '/pages'
        },
    };
    function breadcrumbs_render() {
        let result = m('ul', {class: 'breadcrumb mt-2'}, [
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/'}, m('i', {class: 'fa fa-home'}))),
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/tags'}, 'Список тегов')),
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: `/tag/${data.tag.id}`}, data.tag.name)),
            m('li', {class: 'breadcrumb-item active'}, 'Список статей'),
        ]);
        return result;
    };
    function page_filter(page) {
        let filter = data.filter.data;
        let value = filter.value;
        if ( value.length<1 ) {
            return true;
        }
        if ( page.title.toLowerCase().includes(value.toLowerCase()) ) {
            return true;
        }
        return false;
    };
    function tag_get(id) {
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": 'tag',
                "params": {
                    "id": id
                },
                "id": get_id()
            }
        }).then(
            function(response) {
                if ('result' in response) {
                    data.tag = response['result'];
                    data.pagination.prefix_url = `/tag/${data.tag.id}/pages`;
                    document.title = `Статьи с тегом [${data.tag.name}`;
                    pages_get();
                }
            }
        );
    };
    function pages_get() {
        m.request({
            url: '/api',
            method: "POST",
            body: [
                {
                    "jsonrpc": "2.0",
                    "method": 'tag.pages',
                    "params": {
                        "id": data.tag.id,
                        "page": data.pagination.page,
                        "order_by": data.order_by.value,
                        "fields": ["id", "title", "tags"]
                    },
                    "id": get_id()
                },
                {
                    "jsonrpc": "2.0",
                    "method": 'tag.pages.count',
                    "params": {
                        "id": data.tag.id,
                    },
                    "id": get_id()
                }
            ]

        }).then(
            function(response) {
                if ('result' in response[0]) {
                    data.raw_pages = response[0]['result'];
                }
                if ('result' in response[1]) {
                    data.pagination.size = response[1]['result'];
                }
            }
        );
    }
    return {
        oninit: function(vnode) {
            console.log('TagPages.oninit');
            if (vnode.attrs.page!==undefined) {
                data.pagination.page = Number(vnode.attrs.page);
            };
            tag_get(vnode.attrs.id);
        },
        onbeforeupdate: function(vnode) {
            console.log('TagPages.onbeforeupdate');
            if (vnode.attrs.page!==undefined) {
                if (data.pagination.page.toString() != vnode.attrs.page) {
                    data.pagination.page = Number(vnode.attrs.page);
                    pages_get();
                }
            };
        },
        view: function(vnode) {
            console.log('TagPages.view');
            result = [];
            if (data.tag!=null) {
                result.push(
                    breadcrumbs_render(),
                    m('div', {class: 'row'},
                        m('div', {class: 'col h1 py-2'}, [
                            m('div', {class: "btn-group btn-group-lg me-2"}, [
                                m(m.route.Link, {class: "btn btn-outline-secondary", href: `/tag/${data.tag.id}`, title: "Вернуться"}, m('i', {class: "fa fa-chevron-left"})),
                                m('button', {type: "button", class: "btn btn-outline-secondary", onclick: function() { panel_show(data.filter.data) }},
                                    m('i', {class: "fa fa-filter"})
                                ),
                                m('button', {type: "button", class: "btn btn-outline-secondary", onclick: function() { panel_show(data.order_by.data) }},
                                    m('i', {class: "fa fa-sort-alpha-asc"})
                                )
                            ]),
                            `Статьи с тегом [${data.tag.name}]`
                        ]),
                        m('hr'),
                    )
                );

                // result.push(m(MenuTag, {menuitem: 'pages', tag: data.tag}));
                result.push({tag: MenuTag, attrs: {menuitem: 'pages', tag: data.tag}});

                result.push(m(data.filter));
                result.push(m(data.order_by));
                result.push(m(Pagination, data.pagination));
                if (data.pages.length>0) {
                    result.push(m(ComponentPages, {pages: data.pages}));
                    result.push(m(Pagination, data.pagination));
                };
                result.push(breadcrumbs_render());
            };
            return result;
        }
    }
};
