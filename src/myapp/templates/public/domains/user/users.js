function Users() {
    document.title = `Список пользователей - ${SETTINGS.TITLE}`;
    let data = {
        filter: PanelFilter(),
        order_by: PanelOrderBy({
            field: 'name',
            fields: [
                {value: 'id', text: 'ID'},
                {value: 'name', text: 'имени'},
                {value: 'created', text: 'дате создания'},
                {value: 'updated', text: 'дате обновления'}
            ],
            clickHandler: users_get,
            order: 'asc',
        }),
        raw_users: [],
        get users() {
            /* Отфильтрованный список */
            let value = data.filter.data.value;
            if ( value.length<1 ) {
                return data.raw_users;
            }
            if (data.filter.data.isregex) {
                try {
                    let regex = new RegExp(value, 'ig');
                } catch (e) {
                    console.log(e);
                    return data.raw_users;
                }
            }
            let result = data.raw_users.filter(user_filter);
            return result; 
        },
        pagination: Pagination({
            clickHandler: users_get,
            prefix_url: '/users'
        }),
    };
    function user_filter(user) {
        /* Фильтр статей */
        let value = data.filter.data.value;
        if ( value.length<1 ) {
            return true;
        }
        let isTitle = null;
        if ( data.filter.data.isregex) {
            let regex = new RegExp(value, 'ig');
            isTitle = regex.test(user.name.toLowerCase());
        } else {
            isTitle = user.name.toLowerCase().includes(value.toLowerCase());
        }
        if ( isTitle ) {
            return true;
        }
        return false;
    };
    function breadcrumbs_render() {
        let result = m('ul', {class: 'breadcrumb mt-3'}, [
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/'}, m('i', {class: 'fa fa-home'}))),
            m('li', {class: 'breadcrumb-item active'}, 'Список пользователей'),
        ]);
        return result;
    };
    function users_get() {
        let order_by = data.order_by.data;
        let pagination = data.pagination.data;
        m.request({
            url: '/api',
            method: "POST",
            body: [
                {
                    "jsonrpc": "2.0",
                    "method": 'users',
                    "params": {
                        "page": pagination.page,
                        "order_by": {
                            "field": order_by.field,
                            'order': order_by.order
                        },
                        "fields": ["id", "name"]
                    },
                    "id": 1
                },
                {
                    "jsonrpc": "2.0",
                    "method": 'users.count',
                    "id": 1
                }
            ]

        }).then(
            function(response) {
                if ('result' in response[0]) {
                    data.raw_users = response[0]['result'];
                }
                if ('result' in response[1]) {
                    data.pagination.size = response[1]['result'];
                }
            }
        );
    };
    function user_render(user, userIdx) {
        let odd = '';
        if (userIdx % 2) {
            odd = ' bg-light'
        };
        return m('div', {class: 'row'},
            m('div', {class: `col py-2 ${odd}`}, [
                m(m.route.Link, {href: `/user/${user.id}`}, m.trust(user.name)),
                m('div', {class: 'row'},
                ),
            ])
        );
    };
    function users_render() {
        return data.users.map(user_render);
    };
    return {
        oninit: function(vnode) {
            let pagination = data.pagination.data;
            if (vnode.attrs.page!==undefined) {
                pagination.page = Number(vnode.attrs.page);
            };
            users_get();
        },
        view: function(vnode) {
            let result = [];
            result.push(
                breadcrumbs_render(),
                m('div', {class: 'row'},
                    m('div', {class: 'col h1 py-1'}, [
                        m('div', {class: "btn-group btn-group-lg me-2"}, [
                            m('button', {type: "button", class: "btn btn-outline-secondary", onclick: function() { panel_show(data.filter.data) }},
                                m('i', {class: "fa fa-filter"})
                            ),
                            m('button', {type: "button", class: "btn btn-outline-secondary", onclick: function() { panel_show(data.order_by.data) }},
                                m('i', {class: "fa fa-sort-alpha-asc"})
                            )
                        ]),
                        'Пользователи',
                    ])
                ),
                m('hr'),
            );
            result.push(m(data.filter));
            result.push(m(data.order_by));
            result.push(m(data.pagination));
            if (data.users.length>0) {
                result.push(m(ComponentUsers, {users: data.users}));
                result.push(m(data.pagination));
            };
            result.push(breadcrumbs_render());
            return result
        }
    }
};
