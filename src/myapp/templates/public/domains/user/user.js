function User() {
    let data = {
        user: null,
        panels: {
            standart: {
                visible: false
            },
        },
    };
    function breadcrumbs_render() {
        let result = m('ul', {class: 'breadcrumb mt-3'}, [
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/'}, m('i', {class: 'fa fa-home'}))),
            m('li', {class: 'breadcrumb-item'},  m(m.route.Link, {href: '/users'}, 'Список пользователей')),
            m('li', {class: 'breadcrumb-item active'}, m.trust(data.user.name)),
        ]);
        return result;
    };
    function user_get(id) {
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": 'user',
                "params": {
                    "id": id,
                    "fields": ["id", "name", "created", "disabled"]
                },
                "id": 1
            }
        }).then(
            function(response) {
                if ('result' in response) {
                    data.user = response['result'];
                    document.title = `${data.user.title} - ${SETTINGS.TITLE}`;
                }
            }
        );
    };
    return {
        oninit: function(vnode) {
            console.log('User.oninit');
            user_get(vnode.attrs.id);
        },
        onupdate: function(vnode) {
            console.log('User.onupdate');
            if (data.user!=null) {
                if (data.user.id.toString()!==vnode.attrs.id) {
                    user_get(vnode.attrs.id);
                }
            }
        },
        view: function(vnode) {
            console.log('User.view');
            let result = [];
            if (data.user!=null) {
                result.push(
                    breadcrumbs_render(),
                    m('div', {class: 'row'},
                        m('div', {class: 'col h1 py-1'}, [
                            m('div', {class: "btn-group btn-group-lg me-2"}, [
                                m(m.route.Link, {class: "btn btn-outline-secondary", href: "/users", title: "Список пользователей"}, m('i', {class: 'fa fa-chevron-left'})),
                                m('button', {class: 'btn btn-outline-secondary', title: 'Инструменты', onclick: function() {panel_show(data.panels.standart)}}, m('i', {class: 'fa fa-cog'})),
                            ]),
                            m.trust(data.user.name),
                        ])
                    ),
                    m('hr'),
                    m.trust(data.user.body),
                    breadcrumbs_render(),
                );
            };
            return result
        }
    }
};
