function Pages() {
    let data = {
        filter: PanelFilter(),
        order_by: PanelOrderBy({
            field: 'title',
            fields: [
                {value: 'id', text: 'ID'},
                {value: 'title', text: 'заголовку'},
                {value: 'created', text: 'дате создания'},
                {value: 'updated', text: 'дате обновления'}
            ],
            clickHandler: pages_get,
            order: 'asc',
        }),
        raw_pages: [],
        get pages() {
            /* Отфильтрованный список */
            let value = data.filter.data.value;
            if ( value.length<1 ) {
                return data.raw_pages;
            }
            if (data.filter.data.isregex) {
                try {
                    let regex = new RegExp(value, 'ig');
                } catch (e) {
                    console.log(e);
                    return data.raw_pages;
                }
            }
            let result = data.raw_pages.filter(page_filter);
            return result; 
        },
        pagination: Pagination({
            clickHandler: pages_get,
            prefix_url: '/pages'
        }),
    };
    function page_filter(page) {
        /* Фильтр статей */
        let value = data.filter.data.value;
        if ( value.length<1 ) {
            return true;
        }
        let isTitle = null;
        if ( data.filter.data.isregex) {
            let regex = new RegExp(value, 'ig');
            isTitle = regex.test(page.title.toLowerCase());
        } else {
            isTitle = page.title.toLowerCase().includes(value.toLowerCase());
        }
        if ( isTitle ) {
            return true;
        }
        return false;
    };
    function breadcrumbs_render() {
        let result = m('ul', {class: 'breadcrumb mt-3'}, [
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/'}, m('i', {class: 'fa fa-home'}))),
            m('li', {class: 'breadcrumb-item active'}, 'Список статей'),
        ]);
        return result;
    };
    function pages_get() {
        let order_by = data.order_by.data;
        let pagination = data.pagination.data;
        m.request({
            url: '/api',
            method: "POST",
            body: [
                {
                    "jsonrpc": "2.0",
                    "method": 'pages',
                    "params": {
                        "page": pagination.page,
                        "order_by": {
                            "field": order_by.field,
                            'order': order_by.order
                        },
                        "fields": ["id", "title", "tags"]
                    },
                    "id": 1
                },
                {
                    "jsonrpc": "2.0",
                    "method": 'pages.count',
                    "id": 1
                }
            ]

        }).then(
            function(response) {
                if ('result' in response[0]) {
                    data.raw_pages = response[0]['result'];
                }
                if ('result' in response[1]) {
                    data.pagination.size = response[1]['result'];
                }
            }
        );
    };
    function page_render(page, pageIdx) {
        let odd = '';
        if (pageIdx % 2) {
            odd = ' bg-light'
        };
        return m('div', {class: 'row'},
            m('div', {class: `col py-2 ${odd}`}, [
                m(m.route.Link, {href: `/page/${page.id}`}, m.trust(page.title)),
                m('div', {class: 'row'},
                ),
            ])
        );
    };
    function pages_render() {
        return data.pages.map(page_render);
    };
    return {
        oninit: function(vnode) {
            let pagination = data.pagination.data;
            if (vnode.attrs.page!==undefined) {
                pagination.page = Number(vnode.attrs.page);
            };
            document.title = `Список статей - ${SETTINGS.TITLE}`;
            pages_get();
        },
        view: function(vnode) {
            let result = [];
            result.push(
                breadcrumbs_render(),
                m('div', {class: 'row'},
                    m('div', {class: 'col h1 py-1'}, [
                        m('div', {class: "btn-group btn-group-lg me-2"}, [
                            m('button', {type: "button", class: "btn btn-outline-secondary", onclick: function() { panel_show(data.filter.data) }},
                                m('i', {class: "fa fa-filter"})
                            ),
                            m('button', {type: "button", class: "btn btn-outline-secondary", onclick: function() { panel_show(data.order_by.data) }},
                                m('i', {class: "fa fa-sort-alpha-asc"})
                            )
                        ]),
                        'Статьи',
                    ])
                ),
                m('hr'),
            );
            result.push(m(data.filter));
            result.push(m(data.order_by));
            result.push(m(data.pagination));
            if (data.pages.length>0) {
                result.push(m(ComponentPages, {pages: data.pages}));
                result.push(m(data.pagination));
            };
            result.push(breadcrumbs_render());
            return result
        }
    }
};
/*
<div class="row" v-for="() in pages">
<div class="col py-2" :class="{'bg-light': pageIdx % 2}">
<a :href="'/page/' + page.id">{ page.title }</a>

<div class="row">
<div class="col small text-muted">
<span v-for="(tag, tagIdx) in page.tags">
<i class="fa fa-tag"></i> <a class="text-monospace" :href="'/tag/' + tag.id">{ tag.name }</a>&nbsp;
</span>
</div>
</div>

<div class="row">
<div class="col small text-muted">
<i class="fa fa-user"></i> <a :href="'/user/' + page.user.id">{ page.user.name }</a>&nbsp;
Создано: { page.created }&nbsp;
Обновлено: { page.updated }
</div>
</div>

</div>
</div>
*/
