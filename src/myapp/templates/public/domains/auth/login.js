function Login() {
    let data = {
        username: '',
        password: ''
    };
    function login() {
        if (data.username.length==0 || data.password.length==0) {
            return;
        }
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": 'auth.login',
                "params": {
                    "username": data.username,
                    "password": data.password
                },
                "id": 1
            }
        }).then(
            function(response) {
                if ('result' in response) {
                    window.location.href = '/';
                } else if ('error' in response) {
                    data.error = response['error'];
                }
            }
        );
    };
    function form_submit(e) {
        e.preventDefault();
        login();
    };
    return {
        data: data,
        view: function(vnode) {
            let result = [];
            result.push(
                m('div', {class: 'row justify-content-center my-3'}, 
                    m('div', {class: 'col-md-6'}, [
                        m('h3', [
                            {tag: '<', children: '<a class="btn btn-outline-secondary float-end" href="/forgot-password">Забыл пароль</a>'},
                            'Вход',
                            {tag: '<', children: '<hr />'},
                        ]),
                        m('form', {onsubmit: form_submit}, [
                            m('div', {class: "input-group mb-3"}, [
                                {tag: '<', children: '<span class="input-group-text"><i class="fa fa-user"></i></span>'},
                                m('input', {class: 'form-control', placeholder: 'Логин', type: 'text', oninput: function (e) {data.username = e.target.value}, value: data.username}),
                            ]),
                            m('div', {class: "input-group mb-3"}, [
                                {tag: '<', children: '<span class="input-group-text"><i class="fa fa-lock"></i></span>'},
                                m('input', {class: 'form-control', placeholder: 'Пароль', type: 'password', oninput: function (e) {data.password = e.target.value}, value: data.password}),
                            ]),
                            m('div', {class: 'row'},
                                m('div', {class: "col py-2"}, [
                                    m(m.route.Link, {class: 'btn btn-outline-secondary', href: '/register'}, 'Регистрация'),
                                    m('button', {class: 'btn btn-outline-success float-end', type: 'submit'}, 'Войти') 
                                ]),
                            ),
                        ])
                    ])
                )
            )
            return result;
        }
    };
};
