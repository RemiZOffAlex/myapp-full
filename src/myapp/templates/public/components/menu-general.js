let MenuGeneral = {
    oninit: function(vnode) {
        document.title = SETTINGS.TITLE;
    },
    view: function(vnode) {
        result = [];
        result.push(
            m('div', {class: 'row'},
                m('div', {class: 'col py-2'}, [
                    m(m.route.Link, {class: 'btn btn-outline-secondary btn-lg', href: '/'}, m('i', {class: 'fa fa-home'})),
                    m(m.route.Link, { class: 'btn btn-outline-secondary btn-lg border-0', href: '/pages'}, 'Статьи'),
                    m(m.route.Link, { class: 'btn btn-outline-secondary btn-lg border-0', href: '/tags'}, 'Метки'),
                    m(m.route.Link, { class: 'btn btn-outline-success btn-lg float-end', href: '/login'}, m('i', {class: 'fa fa-sign-in'})),
                ])
            )
        )
        return result;
    }
};
