function ComponentUsers() {
    let data = {
        users: null,
    };
    function user_render(user, userIdx) {
        let odd = '';
        if (userIdx % 2) {
            odd = ' bg-light'
        };
        return m('div', {class: 'row'},
            m('div', {class: "col py-2" + odd}, [
                m(m.route.Link, {class: "text-decoration-none", href: `/user/${user.id}`}, user.name),
            ])
        )
    };
    function users_render() {
        return data.users.map(user_render);
    };
    return {
        oninit: function(vnode) {
            console.log('ComponentUsers.oninit');
            for (let key in vnode.attrs){
                data[key] = vnode.attrs[key];
            };
        },
        onupdate: function(vnode) {
            console.log('ComponentUsers.onupdate');
            for (let key in vnode.attrs){
                data[key] = vnode.attrs[key];
            };
        },
        view: function() {
            console.log('ComponentUsers.view');
            if (data.users!=null) {
                let result = [];
                result.push(users_render());
                return result;
            };
        }
    };
};
