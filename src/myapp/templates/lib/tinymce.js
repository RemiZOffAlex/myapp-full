function tinymce_config_init() {
    return {
        selector: '',
        height: 400,
        language: 'ru',
        plugins: 'anchor autolink charmap code directionality fullscreen image importcss link lists media pagebreak preview save searchreplace table visualblocks visualchars nonbreaking',
        toolbar: 'code | undo redo | styles blocks | bold italic underline strikethrough removeformat | alignleft aligncenter alignright alignjustify | outdent indent | numlist bullist | pagebreak | fullscreen | link image anchor charmap | nonbreaking',

        font_family_formats: 'Liberation Sans=Liberation Sans; Mono=Liberation Mono;',
        formats: {
            img: { selector: 'img', classes: 'img-thumbnail', styles: {} },
            paragraph: { selector: 'p,h1,h2,h3,h4,h5,h6', classes: '', styles: {} },
            table: { selector: 'table', classes: 'table', styles: {} },
            note: { classes: 'alert alert-primary', styles: {} },
            danger: { classes: 'alert alert-danger', styles: {} },
            bold: [
                { inline: 'strong', remove: 'all' },
                { inline: 'span', remove: 'all' },
                { inline: 'b', remove: 'all' }
            ],
        },
        style_formats: [
            { title: 'Картинка', selector: 'img', format: 'img' },
            // { title: 'Абзац', format: 'paragraph' }, // С этим не работает список стилей
            { title: 'Таблица', selector: 'table', format: 'table' },
            { title: 'Примечание', format: 'note' },
            { title: 'Внимание', format: 'danger' },
        ],
        content_css: `${SETTINGS.STATIC}/css/tinymce-code.css`,
        relative_urls: false,
        convert_urls: false,
        // Table
        table_default_attributes: {},
        table_default_styles: {},
        table_class_list: [
            { title: 'default', value: 'table' },
            { title: 'None', value: '' },
        ],
        setup: function (ed) {
            ed.on('change', function (e) {
                console.log('the event object ', e);
                console.log('the editor object ', ed);
                console.log('the content ', ed.getContent());
            });
        },
    };
};
function tinymce_remove(id) {
    tinymce.get(id).remove();
};
