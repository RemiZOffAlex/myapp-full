function EventBus() {
    let listeners = {};
    return {
        on: function(event, callback) {
            console.log('on');
            if (!(event in listeners)) {
                listeners[event] = [];
            }
            listeners[event].push(callback)
        },
        off: function(event, id) {
            console.log('off');
        },
        emit: function(event, arguments) {
            console.log('emit');
            listeners[event].forEach(element => {
                console.log(element);
                let result = element(arguments);
            });
        }
    };
};
