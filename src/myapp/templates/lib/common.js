function arrayRemove(arr, value) {
    /* Удаление элемента из списка */
    return arr.filter(function(ele){
        return ele.id != value.id;
    });
};
function arrayRemoveByID(arr, value) {
    /* Удаление элемента из списка */
    return arr.filter(function (ele) {
        return ele.id != value.id;
    });
};
function arrayRemoveByItem(arr, value) {
    /* Удаление элемента из списка */
    return arr.filter(function (ele) {
        return ele != value;
    });
};
function panel_show(panel) {
    /* Показать/скрыть панель */
    panel.visible = !panel.visible;
};
function get_id() {
    /* Получить рандомный ID */
    return Math.random().toString(16).slice(2);
};
