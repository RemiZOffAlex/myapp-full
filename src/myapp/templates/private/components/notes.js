function ComponentNotes() {
    let data = {
        notes: null,
    };
    function note_render(note, noteIdx) {
        let odd = '';
        if (noteIdx % 2) {
            odd = ' bg-light'
        };
        let tags = note.tags.map(
            function(tag, tagIdx) {
                return [
                    m('i', {class: "fa fa-tag"}),
                    {tag: '<', children: '&nbsp;'},
                    m(m.route.Link, {class: "font-monospace text-decoration-none", href: `/tag/${tag.id}`}, tag.name),
                    {tag: '<', children: '&nbsp;'},
                ]
            }
        );

        return m('div', {class: 'row'},
            m('div', {class: "col py-2" + odd}, [
                m(m.route.Link, {class: "text-decoration-none", href: `/note/${note.id}`}, m.trust(note.title)),
                m('div', {class: 'row'},
                    m('div', {class: 'col text-muted'},
                        m('small', [...tags])
                    )
                )
            ])
        )
    };
    function notes_render() {
        return data.notes.map(note_render);
    };
    return {
        oninit: function(vnode) {
            console.log('ComponentNotes.oninit');
            for (let key in vnode.attrs){
                data[key] = vnode.attrs[key];
            };
        },
        onupdate: function(vnode) {
            console.log('ComponentNotes.onupdate');
            for (let key in vnode.attrs){
                data[key] = vnode.attrs[key];
            };
        },
        view: function() {
            console.log('ComponentNotes.view');
            if (data.notes!=null) {
                let result = [];
                result.push(notes_render());
                return result;
            };
        }
    };
};
