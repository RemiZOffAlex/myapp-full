function ComponentPages() {
    let data = {
        pages: null,
    };
    function page_render(page, pageIdx) {
        let odd = '';
        if (pageIdx % 2) {
            odd = ' bg-light'
        };
        let tags = page.tags.map(
            function(tag, tagIdx) {
                return [
                    m('i', {class: "fa fa-tag"}),
                    {tag: '<', children: '&nbsp;'},
                    m(m.route.Link, {class: "font-monospace text-decoration-none", href: `/tag/${tag.id}`}, tag.name),
                    {tag: '<', children: '&nbsp;'},
                ]
            }
        );

        return m('div', {class: 'row'},
            m('div', {class: "col py-2" + odd}, [
                m(m.route.Link, {class: "text-decoration-none", href: `/page/${page.id}`}, m.trust(page.title)),
                m('div', {class: 'row'},
                    m('div', {class: 'col text-muted'},
                        m('small', [...tags])
                    )
                )
            ])
        )
    };
    function pages_render() {
        return data.pages.map(page_render);
    };
    return {
        oninit: function(vnode) {
            console.log('ComponentPages.oninit');
            for (let key in vnode.attrs){
                data[key] = vnode.attrs[key];
            };
        },
        onupdate: function(vnode) {
            console.log('ComponentPages.onupdate');
            for (let key in vnode.attrs){
                data[key] = vnode.attrs[key];
            };
        },
        view: function() {
            console.log('ComponentPages.view');
            if (data.pages!=null) {
                let result = [];
                result.push(pages_render());
                return result;
            };
        }
    };
};
