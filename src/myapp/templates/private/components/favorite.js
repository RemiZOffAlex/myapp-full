/*
Пример

m(ComponentFavorite, {resource: data.journal, name: 'journal'})
*/
function ComponentFavorite(arguments) {
    let data = {
        resource: null,
        name: null
    };
    function favorite_add() {
        /* Добавить журнал в Избранное */
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": `${data.name}.favorite.add`,
                "params": {
                    "id": data.resource.id
                },
                "id": get_id()
            }
        }).then(
            function(response) {
                if ('result' in response) {
                    data.resource.favorite = true;
                }
            }
        );
    };
    function favorite_delete() {
        /* Удалить журнал из Избранного */
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": `${data.name}.favorite.delete`,
                "params": {
                    "id": data.resource.id
                },
                "id": get_id()
            }
        }).then(
            function(response) {
                if ('result' in response) {
                    data.resource.favorite = false;
                }
            }
        );
    };
    function button_favorite_render() {
        if (data.resource.favorite) {
            return m('button', {class: "btn btn-primary me-2", type: 'button', onclick: favorite_delete, title: "Удалить из избранного"}, m('i', {class: 'fa fa-star'}));
        } else {
            return m('button', {class: "btn btn-outline-secondary me-2", type: 'button', onclick: favorite_add, title: "Добавить в избранное"}, m('i', {class: 'fa fa-star-o'}));
        }
    };
    return {
        oninit: function(vnode) {
            console.log('ComponentTags.oninit');
            for (let key in vnode.attrs){
                data[key] = vnode.attrs[key];
            };
        },
        view: function(vnode) {
            return button_favorite_render();
        }
    };
};
