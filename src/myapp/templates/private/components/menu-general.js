function MenuGeneral() {
    function logout() {
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": 'auth.logout',
                "id": 1
            }
        }).then(
            function(response) {
                if ('result' in response) {
                    window.location.href = '/';
                }
            }
        );
    };
    return {
        oninit: function(vnode) {
            document.title = SETTINGS.TITLE;
        },
        view: function(vnode) {
            result = [];
            result.push(
                m('div', {class: 'row'},
                    m('div', {class: 'col py-2'}, [
                        m(m.route.Link, { class: 'btn btn-outline-secondary btn-lg', href: '/'}, m('i', {class: 'fa fa-home'})),
                        m(m.route.Link, { class: 'btn btn-outline-secondary btn-lg border-0', href: '/pages'}, 'Статьи'),
                        m(m.route.Link, { class: 'btn btn-outline-secondary btn-lg border-0', href: '/tags'}, 'Метки'),
                        m(m.route.Link, { class: 'btn btn-outline-secondary btn-lg border-0', href: '/users'}, 'Пользователи'),
                        m('div', { class: 'btn-group btn-group-lg float-end'},
                            m(m.route.Link, { class: 'btn btn-outline-primary', href: '/notes' }, m('i', { class: 'fa fa-sticky-note-o' })),
                            m(m.route.Link, { class: 'btn btn-outline-warning', href: '/favorite' }, m('i', { class: 'fa fa-star' })),
                            m(m.route.Link, { class: 'btn btn-outline-secondary', href: '/profile'}, m('i', {class: 'fa fa-user'})),
                            m('button', {class: 'btn btn-outline-danger', onclick: logout}, m('i', {class: 'fa fa-sign-out'})),
                        )
                    ])
                )
            )
            return result;
        }
    };
};
