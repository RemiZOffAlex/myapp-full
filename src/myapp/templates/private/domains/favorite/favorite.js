function Favorite() {
    function breadcrumbs_render() {
        let result = m('ul', {class: 'breadcrumb mt-2'}, [
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/'}, m('i', {class: 'fa fa-home'}))),
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/favorite'}, 'Избранное')),
            m('li', {class: 'breadcrumb-item active'}, 'Избранные вопросы'),
        ]);
        return result;
    };
    return {
        view: function(vnode) {
            console.log('Favorite.view');
            let result = [];
            result.push([
                breadcrumbs_render(),
                m('div', {class: 'row'},
                    m('div', {class: 'col h1 py-2'}, [
                        m(m.route.Link, {class: "btn btn-outline-secondary btn-lg me-2", href: '/profile', title: "Вернуться"}, m('i', {class: "fa fa-chevron-left"})),
                        'Избранное'
                    ])
                ),
                m('hr'),
                m(MenuFavorite),
                breadcrumbs_render(),
            ]);
            return result;
        }
    };
};
/*
<h3>
<a class="btn btn-outline-secondary" href="/profile"><i class="fa fa-chevron-left"></i></a>
Избранное</h3>
{# include 'favorite_menu.html' #}
<hr />

<div class='row'>
<div class="col-md-4 py-2">
<a class="btn btn-outline-secondary btn-lg w-100" href="/favorite/questions">Вопросы <span class="badge bg-secondary">{ questions }</span></a>
</div>

<div class="col-md-4 py-2">
<a class="btn btn-outline-secondary btn-lg w-100" href="/favorite/orders">Заказы <span class="badge bg-secondary">{ orders }</span></a>
</div>

<div class="col-md-4 py-2">
<a class="btn btn-outline-secondary btn-lg w-100" href="/favorite/pages">Статьи <span class="badge bg-secondary">{ pages }</span></a>
</div>

<div class="col-md-4 py-2">
<a class="btn btn-outline-secondary btn-lg w-100" href="/favorite/resumes">Резюме <span class="badge bg-secondary">{ resumes }</span></a>
</div>
</div>

Object.assign(root.data, {
    questions: 0,
    orders: 0,
    pages: 0,
    resumes: 0,
    menuitem: null,
});
root.created = function() {
    let vm = this;
        m.request({
            url: '/api',
            method: "POST",
            body: [
                {
                    "jsonrpc": "2.0",
                    "method": 'favorite.questions.count',
                    "params": {},
                    "id": get_id()
                },
                {
                    "jsonrpc": "2.0",
                    "method": 'favorite.orders.count',
                    "params": {},
                    "id": get_id()
                },
                {
                    "jsonrpc": "2.0",
                    "method": 'favorite.pages.count',
                    "params": {},
                    "id": get_id()
                },
                {
                    "jsonrpc": "2.0",
                    "method": 'favorite.resumes.count',
                    "params": {},
                    "id": get_id()
                },
            ]
        }).then(
        function(response) {
            if ('result' in response[0]) {
                vm.questions = response[0]['result'];
            }
            if ('result' in response[1]) {
                vm.orders = response[1]['result'];
            }
            if ('result' in response[2]) {
                vm.pages = response[2]['result'];
            }
            if ('result' in response[3]) {
                vm.resumes = response[3]['result'];
            }
        }
    );
};
*/
