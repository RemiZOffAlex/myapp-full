function FavoriteNotes() {
    let data = {
        filter: PanelFilter(),
        order_by: PanelOrderBy({
            field: 'updated',
            fields: [
                {value: 'id', text: 'ID'},
                {value: 'title', text: 'заголовку'},
                {value: 'created', text: 'дате создания'},
                {value: 'updated', text: 'дате обновления'}
            ],
            clickHandler: notes_get,
            order: 'asc',
        }),
        notes: [],
        pagination: {
            page: 1,
            size: 0,
            prefix_url: '/favorite/notes'
        },
    };
    function breadcrumbs_render() {
        let result = m('ul', {class: 'breadcrumb mt-2'}, [
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/'}, m('i', {class: 'fa fa-home'}))),
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/favorite'}, 'Избранное')),
            m('li', {class: 'breadcrumb-item active'}, 'Избранные заметки'),
        ]);
        return result;
    };
    function notes_get() {
        m.request({
            url: '/api',
            method: "POST",
            body: [
                {
                    "jsonrpc": "2.0",
                    "method": 'favorite.notes',
                    "params": {
                        "page": data.pagination.page,
                        "order_by": data.order_by.value,
                        "fields": ["id", "title", "tags", "created", "updated", "user"]
                    },
                    "id": get_id()
                },
                {
                    "jsonrpc": "2.0",
                    "method": 'favorite.notes.count',
                    "id": get_id()
                }
            ]

        }).then(
            function(response) {
                if ('result' in response[0]) {
                    data.notes = response[0]['result'];
                }
                if ('result' in response[1]) {
                    data.pagination.size = response[1]['result'];
                }
            }
        );
    };
    return {
        oninit: function(vnode) {
            console.log('FavoriteNotes.oninit');
            document.title = `Избранные статьи - ${SETTINGS.TITLE}`;
            if (vnode.attrs.page!==undefined) {
                data.pagination.page = Number(vnode.attrs.page);
            };
            notes_get();
        },
        onbeforeupdate: function(vnode) {
            console.log('FavoriteNotes.onbeforeupdate');
            if (vnode.attrs.page!==undefined) {
                if (data.pagination.page != Number(vnode.attrs.page)) {
                    data.pagination.page = Number(vnode.attrs.page);
                    notes_get();
                };
            } else {
                if (data.pagination.page != 1) {
                    data.pagination.page = 1;
                    notes_get();
                };
            }
        },
        view: function(vnode) {
            console.log('FavoriteNotes.view');
            result = [];
            result.push(
                breadcrumbs_render(),
                m('div', {class: 'row'},
                    m('div', {class: 'col h1 py-2'}, [
                        m('div', {class: "btn-group btn-group-lg me-2"}, [
                            m(m.route.Link, {class: "btn btn-outline-secondary", href: '/favorite', title: "Вернуться"}, m('i', {class: "fa fa-chevron-left"})),
                            m('button', {type: "button", class: "btn btn-outline-secondary", onclick: function() { panel_show(data.filter.data) }},
                                m('i', {class: "fa fa-filter"})
                            ),
                            m('button', {type: "button", class: "btn btn-outline-secondary", onclick: function() { panel_show(data.order_by.data) }},
                                m('i', {class: "fa fa-sort-alpha-asc"})
                            )
                        ]),
                        `Избранные заметки`
                    ])
                ),
                m('hr'),
                m(MenuFavorite, {menuitem: 'notes'}),
            );

            result.push(m(data.filter));
            result.push(m(data.order_by));
            result.push(m(Pagination, data.pagination));
            if (data.notes.length>0) {
                result.push(m(ComponentNotes, {notes: data.notes}));
                result.push(m(Pagination, data.pagination));
            };
            result.push(breadcrumbs_render());
            return result;
        }
    };
};
