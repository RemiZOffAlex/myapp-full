function MenuFavorite() {
    let data = {
        menuitem: null,
    }
    function button_common() {
        if (data.menuitem===null) {
            return {tag: '<', children: '<div class="btn btn-primary me-2"><i class="fa fa-bars"></i></div>'};
        } else {
            return m(m.route.Link, {class: "btn btn-outline-secondary me-2 text-decoration-none", href: '/favorite', title: 'Избранное'}, m('i', {class: 'fa fa-bars'}))
        }
    };
    function button_pages() {
        if (data.menuitem==='pages') {
            return {tag: '<', children: '<div class="btn btn-primary me-2">Статьи</div>'};
        } else {
            return m(m.route.Link, {class: "btn btn-outline-secondary me-2 text-decoration-none", href: '/favorite/pages'}, 'Статьи')
        }
    };
    function button_notes() {
        if (data.menuitem === 'notes') {
            return { tag: '<', children: '<div class="btn btn-primary me-2">Заметки</div>' };
        } else {
            return m(m.route.Link, { class: "btn btn-outline-secondary me-2 text-decoration-none", href: '/favorite/notes' }, 'Заметки')
        }
    };
    return {
        oninit: function(vnode) {
            console.log('MenuFavorite.oninit');
            for (let key in vnode.attrs){
                console.log(key);
                data[key] = vnode.attrs[key];
            };
        },
        view: function() {
            console.log('MenuFavorite.view');
            return m('div', {class: 'row'},
                m('div', {class: 'col py-2'}, [
                    button_common(),
                    button_notes(),
                    button_pages(),
                ])
            );
        }
    }
};
