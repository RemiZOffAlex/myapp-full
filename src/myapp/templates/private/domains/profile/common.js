function Profile() {
    let data = {
        user: null,
        panels: {
            standart: {
                visible: false
            },
        },
    };
    function breadcrumbs_render() {
        let result = m('ul', {class: 'breadcrumb mt-3'}, [
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/'}, m('i', {class: 'fa fa-home'}))),
            m('li', {class: 'breadcrumb-item active'}, 'Профиль пользователя'),
        ]);
        return result;
    };
    function profile_get() {
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": 'profile',
                "params": {
                    "fields": ["id", "name", "created", "disabled"]
                },
                "id": 1
            }
        }).then(
            function(response) {
                if ('result' in response) {
                    data.user = response['result'];
                }
            }
        );
    };
    return {
        oninit: function(vnode) {
            console.log('Profile.oninit');
            document.title = `Мой профиль - ${SETTINGS.TITLE}`;
            profile_get();
        },
        view: function(vnode) {
            console.log('Profile.view');
            let result = [];
            if (data.user!=null) {
                result.push(
                    breadcrumbs_render(),
                    m('div', {class: 'row'},
                        m('div', {class: 'col h1 py-1'}, [
                            m('div', {class: "btn-group btn-group-lg me-2"}, [
                                m('button', {class: 'btn btn-outline-secondary', title: 'Инструменты', onclick: function() {panel_show(data.panels.standart)}}, m('i', {class: 'fa fa-cog'})),
                            ]),
                            'Мой профиль',
                        ])
                    ),
                    m('hr'),
                );
                result.push(data.user.name);
                result.push(breadcrumbs_render());
            };
            return result
        }
    }
};
