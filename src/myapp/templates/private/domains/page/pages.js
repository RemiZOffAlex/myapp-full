function Pages() {
    let data = {
        filter: PanelFilter(),
        order_by: PanelOrderBy({
            field: 'title',
            fields: [
                {value: 'id', text: 'ID'},
                {value: 'title', text: 'заголовку'},
                {value: 'created', text: 'дате создания'},
                {value: 'updated', text: 'дате обновления'}
            ],
            clickHandler: pages_get,
            order: 'asc',
        }),
        raw_pages: [],
        get pages() {
            let result = data.raw_pages.filter(page_filter);
            return result;
        },
        pagination: Pagination({
            clickHandler: pages_get,
            prefix_url: '/pages'
        }),
    };
    function breadcrumbs_render() {
        let result = m('ul', {class: 'breadcrumb mt-3'}, [
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/'}, m('i', {class: 'fa fa-home'}))),
            m('li', {class: 'breadcrumb-item active'}, 'Список статей'),
        ]);
        return result;
    };
    function page_filter(page) {
        let filter = data.filter.data;
        let value = filter.value;
        if (value.length < 1) {
            return true;
        }
        if (page.title.toLowerCase().includes(value.toLowerCase())) {
            return true;
        }
        return false;
    };
    function pages_get() {
        let pagination = data.pagination.data;
        m.request({
            url: '/api',
            method: "POST",
            body: [
                {
                    "jsonrpc": "2.0",
                    "method": 'pages',
                    "params": {
                        "page": pagination.page,
                        "order_by": {
                            "field": data.order_by.data.field,
                            'order': data.order_by.data.order
                        },
                        "fields": ["id", "title", "tags"]
                    },
                    "id": 1
                },
                {
                    "jsonrpc": "2.0",
                    "method": 'pages.count',
                    "id": 1
                }
            ]

        }).then(
            function(response) {
                if ('result' in response[0]) {
                    data.raw_pages = response[0]['result'];
                }
                if ('result' in response[1]) {
                    data.pagination.size = response[1]['result'];
                }
            }
        );
    };
    function page_render(page, pageIdx) {
        let odd = '';
        if (pageIdx % 2) {
            odd = ' bg-light'
        };
        return m('div', {class: 'row'},
            m('div', {class: `col py-2 ${odd}`}, [
                m(m.route.Link, {href: `/page/${page.id}`}, m.trust(page.title)),
                m('div', {class: 'row'},
                ),
            ])
        );
    };
    function pages_render() {
        return data.pages.map(page_render);
    };
    return {
        oninit: function(vnode) {
            let pagination = data.pagination.data;
            if (vnode.attrs.page!==undefined) {
                pagination.page = Number(vnode.attrs.page);
            };
            document.title = `Список статей - ${SETTINGS.TITLE}`;
            pages_get();
        },
        view: function(vnode) {
            let result = [];
            result.push(
                breadcrumbs_render(),
                m('div', {class: 'row'},
                    m('div', {class: 'col h1 py-1'}, [
                        m(m.route.Link, {class: "btn btn-outline-success btn-lg float-end", href: "/page/add", title: "Добавить статью"}, m('i', {class: 'fa fa-plus'})),
                        m('div', {class: "btn-group btn-group-lg me-2"}, [
                            m('button', {type: "button", class: "btn btn-outline-secondary", onclick: function() { panel_show(data.filter.data) }},
                                m('i', {class: "fa fa-filter"})
                            ),
                            m('button', {type: "button", class: "btn btn-outline-secondary", onclick: function() { panel_show(data.order_by.data) }},
                                m('i', {class: "fa fa-sort-alpha-asc"})
                            )
                        ]),
                        'Статьи',
                        {tag: '<', children: '<hr />'}
                    ])
                )
            );
            result.push(m(data.filter));
            result.push(m(data.order_by));
            result.push(m(data.pagination));
            if (data.pages.length>0) {
                result.push(m(ComponentPages, {pages: data.pages}));
                result.push(m(data.pagination));
            };
            result.push(breadcrumbs_render());
            return result
        }
    }
};
