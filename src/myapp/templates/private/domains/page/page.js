function Page() {
    let data = {
        page: null,
        panels: {
            standart: {
                visible: false
            },
        },
    };
    function breadcrumbs_render() {
        let result = m('ul', {class: 'breadcrumb mt-3'}, [
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/'}, m('i', {class: 'fa fa-home'}))),
            m('li', {class: 'breadcrumb-item'},  m(m.route.Link, {href: '/pages'}, 'Список статей')),
            m('li', {class: 'breadcrumb-item active'}, m.trust(data.page.title)),
        ]);
        return result;
    };
    function page_get(id) {
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": 'page',
                "params": {
                    "id": id,
                    "fields": ["id", "title", "body", "parent_id", "created", "updated", "tags"]
                },
                "id": 1
            }
        }).then(
            function(response) {
                if ('result' in response) {
                    data.page = response['result'];
                    document.title = `${data.page.title} - ${SETTINGS.TITLE}`;
                }
            }
        );
    };
    function page_prev() {
        if (data.page.parent_id) {
            return m(m.route.Link, {class: "btn btn-outline-secondary", href: `/page/${data.page.parent_id}`, title: "Список статей"}, m('i', {class: 'fa fa-chevron-left'}));
        } else {
            return m(m.route.Link, {class: "btn btn-outline-secondary", href: "/pages", title: "Список статей"}, m('i', {class: 'fa fa-chevron-left'}));
        }
    };
    return {
        oninit: function(vnode) {
            console.log('Page.oninit');
            page_get(vnode.attrs.id);
        },
        onupdate: function(vnode) {
            console.log('Page.onupdate');
            if (data.page!=null) {
                if (data.page.id.toString()!==vnode.attrs.id) {
                    page_get(vnode.attrs.id);
                }
            }
        },
        view: function(vnode) {
            console.log('Page.view');
            let result = [];
            if (data.page!=null) {
                result.push(
                    breadcrumbs_render(),
                    m('div', {class: 'row'},
                        m('div', {class: 'col h1 py-1'}, [
                            m('div', {class: "btn-group btn-group-lg me-2"}, [
                                page_prev(),
                                m('button', {class: 'btn btn-outline-secondary', title: 'Инструменты', onclick: function() {panel_show(data.panels.standart)}}, m('i', {class: 'fa fa-cog'})),
                            ]),
                            m.trust(data.page.title),
                        ])
                    ),
                    m('hr'),
                );
                result.push(m.trust(data.page.body));
                result.push(breadcrumbs_render());
            };
            return result
        }
    }
};
