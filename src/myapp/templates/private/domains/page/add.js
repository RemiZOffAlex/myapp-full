function PageAdd() {
    let data = {
        page: {
            title: '',
            body: '',
        },
        editor: null,
    };
    function breadcrumbs_render() {
        let result = m('ul', {class: 'breadcrumb mt-3'}, [
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/'}, m('i', {class: 'fa fa-home'}))),
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/pages'}, 'Список статей')),
            m('li', {class: 'breadcrumb-item active'}, 'Новая статья'),
        ]);
        return result;
    };
    function form_submit(e) {
        e.preventDefault();
        page_add();
    };
    function page_add() {
        if (data.page.title.length<2) {
            return;
        };
        tinymce_get_value();
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": 'page.add',
                "params": data.page,
                "id": 1
            }
        }).then(
            function(response) {
                if ('result' in response) {
                    m.route.set(`/page/${response['result'].id}`);
                }
            }
        )
    };
    function tinymce_remove() {
        tinymce.get('page_body').remove();
    };
    function tinymce_get_value() {
        let value = tinymce.get('page_body').getContent();
        if (value != data.page.body) {
            data.page.body = value;
        }
    };
    function tinymce_set_value() {
        tinymce.get('page_body').setContent(data.page.body);
    };
    return {
        oncreate(vnode) {
            console.log('PageAdd.oncreate');
            if (data.editor==null) {
                data.editor = tinymce_init('#page_body');
            }
        },
        onbeforeremove: function(vnode) {
            console.log('PageAdd.onbeforeremove');
            if (data.editor!=null) {
                tinymce_remove();
            };
        },
        view: function(vnode) {
            console.log('PageAdd.view');
            result = [];
            result.push([
                m('div', {class: 'row'},
                    m('div', {class: 'col py-2 h1'}, [
                        m(m.route.Link, {class: "btn btn-outline-secondary", href: "/pages", title: "Список статей"}, m('i', {class: 'fa fa-chevron-left'})),
                        'Новая страница'
                    ])
                ),
                m('hr')
            ]);
            result.push(
                m('form', {onsubmit: form_submit}, [
                    m('div', {class: 'mb-2'}, [
                        m('label', {class: 'form-label'}, 'Заголовок'),
                        m('input', {class: 'form-control', type: 'text', oninput: function (e) {data.page.title = e.target.value}, value: data.page.title}),
                    ]),
                    m('div', {class: 'mb-2'}, [
                        m('label', {class: 'form-label'}, 'Текст'),
                        m('textarea', {class: 'form-control', cols: '40', rows: '8', id: 'page_body', oninput: function (e) {data.page.body = e.target.value}, value: data.page.body})
                    ]),
                    m('div', {class: 'row'},
                        m('div', {class: 'col py-2'}, [
                            m('button', {class: 'btn btn-outline-success btn-lg float-end', type: 'submit'}, [m('i', {class: 'fa fa-save'}), ' Сохранить']),
                        ])
                    ),
                ])
            );
            result.push(breadcrumbs_render());
            return result;
        }
    };
};
