function NoteAdd() {
    let data = {
        uuid: get_id(),
        note: {
            title: '',
            body: '',
        },
        editor: null,
    };
    function breadcrumbs_render() {
        let result = m('ul', {class: 'breadcrumb mt-2'}, [
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/'}, m('i', {class: 'fa fa-home'}))),
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/notes'}, 'Список заметок')),
            m('li', {class: 'breadcrumb-item active'}, 'Новая заметка'),
        ]);
        return result;
    };
    function form_submit(e) {
        e.preventDefault();
        note_add();
    };
    function note_add() {
        if (data.note.title.length<2) {
            return;
        };
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": 'note.add',
                "params": data.note,
                "id": get_id()
            }
        }).then(
            function(response) {
                if ('result' in response) {
                    m.route.set(`/note/${response['result'].id}`);
                }
            }
        )
    };
    function editor_events(ed) {
        ed.on('change', function (e) {
            data.note.body = ed.getContent();
        });
    };
    return {
        oncreate(vnode) {
            console.log('NoteAdd.oncreate');
            if (data.editor==null) {
                tinymce_config = tinymce_config_init();
                tinymce_config.selector = `#note_body_${data.uuid}`;
                tinymce_config.setup = editor_events;
                tinymce.init(tinymce_config).then(
                    function (editors) {
                        data.editor = editors[0];
                    }
                );
            }
        },
        onbeforeremove: function(vnode) {
            console.log('NoteAdd.onbeforeremove');
            if (data.editor!=null) {
                data.editor.remove();
                data.editor = null;
            };
        },
        view: function(vnode) {
            console.log('NoteAdd.view');
            result = [];
            result.push(breadcrumbs_render());
            result.push([
                m('div', {class: 'row'},
                    m('div', {class: 'col h1 py-2'}, [
                        m(m.route.Link, {class: "btn btn-outline-secondary btn-lg me-2", href: "/notes", title: "Список заметок"}, m('i', {class: 'fa fa-chevron-left'})),
                        'Новая заметка'
                    ])
                ),
                m('hr')
            ]);
            result.push(
                m('form', {onsubmit: form_submit}, [
                    m('div', {class: 'mb-2'}, [
                        m('label', {class: 'form-label'}, 'Заголовок'),
                        m('input', {class: 'form-control', type: 'text', oninput: function (e) {data.note.title = e.target.value}, value: data.note.title}, 'Заголовок'),
                    ]),
                    m('div', {class: 'mb-2'}, [
                        m('label', {class: 'form-label'}, 'Текст'),
                        m('textarea', { class: 'form-control', cols: '40', rows: '8', id: `note_body_${data.uuid}`, oninput: function (e) {data.note.body = e.target.value}, value: data.note.body})
                    ]),
                    m('div', {class: 'row'},
                        m('div', {class: 'col py-2'}, [
                            m('button', {class: 'btn btn-outline-success btn-lg float-end', type: 'submit'}, [m('i', {class: 'fa fa-save me-2'}), 'Сохранить']),
                        ])
                    ),
                ])
            );
            result.push(breadcrumbs_render());
            return result;
        }
    };
};
