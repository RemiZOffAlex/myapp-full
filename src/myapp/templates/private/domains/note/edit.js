function NoteEdit() {
    let data = {
        uuid: get_id(),
        note: null,
        editor: null,
    };
    function breadcrumbs_render() {
        let result = m('ul', {class: 'breadcrumb mt-2'}, [
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/'}, m('i', {class: 'fa fa-home'}))),
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/notes'}, 'Список статей')),
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: `/note/${data.note.id}`}, data.note.title)),
            m('li', {class: 'breadcrumb-item active'}, 'Редактирование страницы'),
        ]);
        return result;
    };
    function note_get(id) {
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": 'note',
                "params": {
                    "id": id,
                    "fields": ["id", "title", "body"]
                },
                "id": get_id()
            }
        }).then(
            function(response) {
                if ('result' in response) {
                    data.note = response['result'];
                }
            }
        )
    };
    function apply() {
        /* Сохранить */
        if (data.note.title.length<2) {
            return;
        };
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": "note.update",
                "params": {
                    "id": data.note.id,
                    "title": data.note.title,
                    "body": data.note.body
                },
                "id": get_id()
            }
        }).then(
            function(response) {
                if ('result' in response) {
                }
            }
        );
    };
    function apply_and_close() {
        /* Сохранить */
        if (data.note.title.length<2) {
            return;
        };
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": "note.update",
                "params": {
                    "id": data.note.id,
                    "title": data.note.title,
                    "body": data.note.body
                },
                "id": get_id()
            }
        }).then(
            function(response) {
                if ('result' in response) {
                    m.route.set(`/note/${data.note.id}`);
                }
            }
        );
    };
    function form_submit(e) {
        e.preventDefault();
        apply_and_close();
    };
    function editor_events(ed) {
        ed.on('change', function (e) {
            data.note.body = ed.getContent();
        });
    };
    return {
        oncreate(vnode) {
            console.log('NoteEdit.oncreate');
            if (data.note!=null) {
                console.log(data.note);
                if (data.editor==null) {
                    tinymce_config = tinymce_config_init();
                    tinymce_config.selector = `#note_body_${data.uuid}`;
                    tinymce_config.setup = editor_events;
                    tinymce.init(tinymce_config).then(
                        function (editors) {
                            data.editor = editors[0];
                        }
                    );
                }
            }
        },
        oninit: function(vnode) {
            console.log('NoteEdit.oninit');
            note_get(vnode.attrs.id);
        },
        onupdate: function(vnode) {
            console.log('NoteEdit.onupdate');
            if (data.note.id.toString()!==vnode.attrs.id) {
                note_get(vnode.attrs.id);
            };
            if (data.note!=null) {
                if (data.editor==null) {
                    tinymce_config = tinymce_config_init();
                    tinymce_config.selector = `#note_body_${data.uuid}`;
                    tinymce_config.setup = editor_events;
                    tinymce.init(tinymce_config).then(
                        function (editors) {
                            data.editor = editors[0];
                        }
                    );
                };
            };
        },
        view: function(vnode) {
            let result = [];
            if (data.note!=null) {
                result.push(
                    breadcrumbs_render(),
                    m('div', {class: 'row'},
                        m('div', {class: 'col h1 py-2'},
                            m(m.route.Link, {class: "btn btn-outline-secondary btn-lg me-2", href: `/note/${data.note.id}`, title: data.note.title}, m('i', {class: 'fa fa-chevron-left'})),
                            'Редактирование страницы',
                        )
                    ),
                    m('hr'),
                    m('form', {onsubmit: form_submit}, [
                        m('div', {class: 'mb-2'}, [
                            m('label', {class: 'form-label'}, 'Заголовок'),
                            m('input', {class: 'form-control', type: 'text', oninput: function (e) {data.note.title = e.target.value}, value: data.note.title}, 'Заголовок'),
                        ]),
                        m('div', {class: 'mb-2'}, [
                            m('label', {class: 'form-label'}, 'Текст'),
                            m('textarea', { class: 'form-control', cols: '40', rows: '8', id: `note_body_${data.uuid}`, oninput: function (e) {data.note.body = e.target.value}, value: data.note.body})
                        ]),
                        m('div', {class: 'row'},
                            m('div', {class: 'col py-2'}, [
                                m('button', { class: 'btn btn-outline-success btn-lg float-end', type: 'button', onclick: apply }, [m('i', { class: 'fa fa-save me-2'}), 'Сохранить']),
                                m('button', {class: 'btn btn-outline-success btn-lg float-end', type: 'submit'}, [m('i', {class: 'fa fa-save me-2'}), 'Сохранить и закрыть']),
                            ])
                        ),
                    ])
                );
                result.push(breadcrumbs_render());
            };
            return result;
        }
    };
};
