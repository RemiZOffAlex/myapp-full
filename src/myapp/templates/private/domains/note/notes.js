function Notes() {
    let data = {
        filter: PanelFilter(),
        order_by: PanelOrderBy({
            field: 'title',
            fields: [
                {value: 'id', text: 'ID'},
                {value: 'title', text: 'заголовку'},
                {value: 'created', text: 'дате создания'},
                {value: 'updated', text: 'дате обновления'}
            ],
            clickHandler: notes_get,
            order: 'asc',
        }),
        notes: [],
        pagination: {
            page: 1,
            size: 0,
            prefix_url: '/notes'
        },
    }
    function breadcrumbs_render() {
        return m('ul', {class: 'breadcrumb mt-2'}, [
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/'}, m('i', {class: 'fa fa-home'}))),
            m('li', {class: 'breadcrumb-item active'}, 'Список заметок')
        ]);
    };
    function notes_get() {
        m.request({
            url: '/api',
            method: "POST",
            body: [
                {
                    "jsonrpc": "2.0",
                    "method": 'notes',
                    "params": {
                        "page": data.pagination.page,
                        "order_by": data.order_by.value,
                        "fields": ["id", "title", "tags", "created", "updated"]
                    },
                    "id": get_id()
                },
                {
                    "jsonrpc": "2.0",
                    "method": 'notes.count',
                    "params": {},
                    "id": get_id()
                }
            ]

        }).then(
            function(response) {
                if ('result' in response[0]) {
                    data.notes = response[0]['result'];
                }
                if ('result' in response[1]) {
                    data.pagination.size = response[1]['result'];
                }
            }
        );
    }
    return {
        oninit: function(vnode) {
            console.log('Notes.oninit');
            document.title = `Список статей - ${SETTINGS.TITLE}`;
            if (vnode.attrs.page!==undefined) {
                data.pagination.page = Number(vnode.attrs.page);
            };
            notes_get();
        },
        onbeforeupdate: function(vnode) {
            console.log('Notes.onbeforeupdate');
            if (vnode.attrs.page!==undefined) {
                if (data.pagination.page.toString() != vnode.attrs.page) {
                    data.pagination.page = Number(vnode.attrs.page);
                    notes_get();
                }
            };
        },
        view: function(vnode) {
            console.log('Notes.view');
            result = [];
            result.push([
                breadcrumbs_render(),
                m('div', {class: 'row'},
                    m('div', {class: 'col py-2 h1'}, [
                        m(m.route.Link, {class: 'btn btn-outline-success btn-lg float-end', href: '/note/add'}, m('i', {class: 'fa fa-plus'})),
                        m('div', {class: "btn-group btn-group-lg me-2"}, [
                            m('button', {type: "button", class: "btn btn-outline-secondary", onclick: function() { panel_show(data.filter.data) }},
                                m('i', {class: "fa fa-filter"})
                            ),
                            m('button', {type: "button", class: "btn btn-outline-secondary", onclick: function() { panel_show(data.order_by.data) }},
                                m('i', {class: "fa fa-sort-alpha-asc"})
                            )
                        ]),
                        'Мои заметки',
                    ])
                ),
                m('hr')
            ]);
            result.push(m(data.filter));
            result.push(m(data.order_by));
            result.push(m(Pagination, data.pagination));
            if (data.notes.length>0) {
                result.push(m(ComponentNotes, {notes: data.notes}));
                result.push(m(Pagination, data.pagination));
            };
            result.push(breadcrumbs_render());
            return result;
        }
    }
};
