function Note() {
    let data = {
        note: null,
        panels: {
            standart: {
                visible: false
            }
        },
    };
    function breadcrumbs_render() {
        let result = m('ul', {class: 'breadcrumb mt-2'}, [
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/'}, m('i', {class: 'fa fa-home'}))),
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/notes'}, 'Список заметок')),
            m('li', {class: 'breadcrumb-item active'}, data.note.title),
        ]);
        return result;
    };
    function settings_save(name) {
        if (name==='status') {
            m.request({
                url: '/api',
                method: "POST",
                body: {
                    "jsonrpc": "2.0",
                    "method": 'note.status',
                    "params": {
                        "id": data.note.id,
                        "status": data.note.status
                    },
                    "id": get_id()
                }
            }).then(
                function(response) {
                    if ('result' in response) {
                        data.note = response['result'];
                    }
                }
            );
        } else if (name==='typeOf') {
            m.request({
                url: '/api',
                method: "POST",
                body: {
                    "jsonrpc": "2.0",
                    "method": 'note.settings.update',
                    "params": {
                        "id": data.note.id,
                        "name": name,
                        "typeOf": "string",
                        "value": value
                    },
                    "id": get_id()
                }
            }).then(
                function(response) {
                    if ('result' in response) {
                        data.note = response['result'];
                    }
                }
            );
        }
    };
    function note_delete() {
        /* Удалить статью в корзину */
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": 'note.delete',
                "params": {
                    "id": data.note.id
                },
                "id": get_id()
            }
        }).then(
            function(response) {
                if ('result' in response) {
                    m.route.set('/notes');
                }
            }
        );
    };
    function note_get(id) {
        m.request({
            url: '/api',
            method: "POST",
            body: [
                {
                    "jsonrpc": "2.0",
                    "method": 'note',
                    "params": {
                        "id": id,
                        "fields": ["id", "title", "body", "created", "updated", "parent_id", "tags", "user", "nodes"]
                    },
                    "id": get_id()
                },
                {
                    "jsonrpc": "2.0",
                    "method": 'note.nodes',
                    "params": {
                        "id": id
                    },
                    "id": get_id()
                }
            ]
        }).then(
            function(response) {
                if ('result' in response[0]) {
                    data.note = response[0]['result'];
                    document.title = `${data.note.title} - ${SETTINGS.TITLE}`;
                }
            }
        )
    };
    function button_back_render() {
        if (data.note.parent_id===null) {
            return m(m.route.Link, {class: "btn btn-outline-secondary", href: "/notes", title: "Список статей"}, m('i', {class: 'fa fa-chevron-left'}))
        } else {
            return m(m.route.Link, {class: "btn btn-outline-secondary", href: `/note/${data.note.parent_id}`, title: "Список статей"}, m('i', {class: 'fa fa-chevron-left'}))
        }
    };
    function button_draft_render() {
        if (data.note.status==='draft') {
            return m('span', {class: 'small text-muted'}, 'черновик')
        }
    };
    function button_trash_render() {
        if (data.note.trash) {
            return m('button', {class: 'btn btn-outline-success', onclick: note_recovery}, [m('i', {class: 'fa fa-plus'}), ' Восстановить из корзины'])
        } else {
            return m('button', {class: 'btn btn-outline-warning', onclick: note_delete}, [m('i', {class: 'fa fa-trash-o'}), ' В корзину'])
        }
    };
    function panels_standart_render() {
        if (data.panels.standart.visible) {
            return m('div', {class: 'row'},
                m('div', {class: 'col py-2'}, [
                    m(ComponentFavorite, {resource: data.note, name: 'note'}),
                    m(m.route.Link, {class: 'btn btn-outline-secondary', href: `/note/${data.note.id}/edit`}, [m('i', {class: 'fa fa-edit'}), ' Редактировать']),
                    m(m.route.Link, {class: 'btn btn-outline-secondary', href: `/note/${data.note.id}/moving`}, [m('i', {class: 'fa fa-arrows'}), ' Перемещение']),
                    m(m.route.Link, {class: 'btn btn-outline-secondary', href: `/note/${data.note.id}/history`}, [m('i', {class: 'fa fa-history'}), ' История изменений']),
                    m(m.route.Link, {class: 'btn btn-outline-secondary', href: `/note/${data.note.id}/source`}, [m('i', {class: 'fa fa-code'}), ' Исходный код']),
                    m(m.route.Link, {class: 'btn btn-outline-secondary', href: `/note/${data.note.id}/print`}, [m('i', {class: 'fa fa-print'}), ' Печать']),
                    m(m.route.Link, {class: 'btn btn-outline-secondary', href: `/note/${data.note.id}/recovery`}, [m('i', {class: 'fa fa-print'}), ' Печать']),
                    button_trash_render(),
                    m('h3', 'Свойства'),
                    m('div', {class: 'row py-2'},
                        m('div', {class: 'col-md-4'}, m('label', 'Статус')),
                        m('div', {class: 'col-md-8'},
                            m('div', {class: 'input-group'}, [
                                m('button', {class: 'btn btn-outline-warning', type: 'button'}, m('i', {class: 'fa fa-rotate-left'})),
                                m('select', {class: 'form-select', onchange: function(e) { data.note.status = e.target.value; }, value: data.note.status}, [
                                    m('option', {value: 'draft'}, 'Черновик'),
                                    m('option', {value: 'published'}, 'Опубликовано')
                                ]),
                                m('button', {class: 'btn btn-outline-success', type: 'button', onclick: function() { settings_save('status') }}, m('i', {class: 'fa fa-save'}))
                            ])
                        )
                    )
                ])
            );
        }
    };
    return {
        oninit: function(vnode) {
            console.log('Note.oninit');
            note_get(Number(vnode.attrs.id));
        },
        onbeforeupdate: function(vnode) {
            console.log('Note.onbeforeupdate');
            if (data.note != null) {
                if (data.note.id!==Number(vnode.attrs.id)) {
                    note_get(Number(vnode.attrs.id));
                }
            }
        },
        view: function(vnode) {
            console.log('Note.view');
            result = [];
            if (data.note!=null) {
                result.push(
                    breadcrumbs_render(),
                    m('div', {class: 'row'},
                        m('div', {class: 'col h1 py-2'},
                            m(m.route.Link, {class: 'btn btn-outline-success float-end', href: `/note/${data.note.id}/add`}, m('i', {class: 'fa fa-plus'})),
                            m('div', {class: 'btn-group btn-group-lg me-2'}, [
                                button_back_render(),
                                m('button', {type: 'button', class: 'btn btn-outline-secondary', onclick: function() {panel_show(data.panels.standart)}}, m('i', {class: 'fa fa-cog'})),
                            ]),
                            data.note.title,
                            button_draft_render(),
                        )
                    ),
                    panels_standart_render(),
                    m('hr'),
                    m(ComponentInfo, {item: data.note}),
                    m(ComponentTags, {resource: data.note, typeOf: 'note'}),
                    m('div', {class: 'row'},
                        m('div', {class: 'col'}, m.trust(data.note.body))
                    ),
                );
                if (data.note.nodes.length>0) {
                    let nodes = data.note.nodes.map(
                        function(subnote, subnoteIdx) {
                            return m('li',
                                m(m.route.Link, {href: `/note/${subnote.id}`}, subnote.title)
                            );
                        }
                    );
                    result.push(
                        m('ul', [...nodes])
                    );
                }
                result.push(breadcrumbs_render());
            }
            return result;
        }
    }
};
