function NoteSource() {
    let data = {
        note: null,
        editor: null,
    }
    function note_get(id) {
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": 'note',
                "params": {
                    "id": id
                },
                "id": get_id()
            }
        }).then(
            function(response) {
                if ('result' in response) {
                    data.note = response['result'];
                }
            }
        )
    }
    function note_update() {
        data.note.body = data.editor.getValue();
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": 'note.update',
                "params": {
                    "id": data.note.id,
                    "title": data.note.title,
                    "body": data.note.body
                },
                "id": get_id()
            }
        }).then(
            function(response) {
                if ('result' in response) {
                    m.route.set(`/note/${response['result'].id}`);
                }
            }
        )
    }
    function editor_init() {
        let vCode = document.getElementById('note_body');
        const {EditorState} = CM["@codemirror/state"];
        const {basicSetup, EditorView} = CM["codemirror"];

        data.editor = {};
        data.editor.state = EditorState.create({
            doc: data.note.body,
            extensions: [
                basicSetup,
                EditorView.updateListener.of(function(e) {
                    data.note.body = e.state.doc.toString();
                }),
                EditorView.lineWrapping
            ]
        });
        data.editor.view = new EditorView({
            lineWrapping: true,
            state: data.editor.state,
            parent: vCode
        });
    };
    function editor_remove() {
        data.editor = null;
    }
    return {
        oncreate(vnode) {
            console.log(vnode);
            if (data.note!=null) {
                console.log(data.note);
                editor_init();
            }
        },
        onbeforeremove: function(vnode) {
            editor_remove();
        },
        oninit: function(vnode) {
            document.title = `Исходный код статьи - ${SETTINGS.TITLE}`;
            note_get(vnode.attrs.id);
        },
        onupdate(vnode) {
            console.log(vnode);
            if (data.note!=null) {
                if (data.editor==null) {
                    console.log(data.note);
                    editor_init();
                }
            }
        },
        view: function(vnode) {
            let result = [];
            if (data.note!=null) {
                result.push([
                    m('div', {class: 'row'},
                        m('div', {class: 'col h1 py-1'}, [
                            m(m.route.Link, {class: "btn btn-outline-secondary btn-lg", href: `/note/${data.note.id}`, title: data.note.title}, m('i', {class: 'fa fa-chevron-left'})),
                            {tag: '<', children: '&nbsp;'},
                            'Исходный код статьи',
                            m('hr'),
                        ])
                    ),
                    m(NoteMenu, {menuitem: 'source', note: data.note}),
                    m('div', {class: 'row'},
                        m('div', {class: 'col py-2'}, [
                            m('button', {class: 'btn btn-outline-success float-end', type: 'button', onclick: note_update}, [m('i', {class: 'fa fa-save'}), ' Сохранить']),
                        ])
                    ),
                     m('div', {class: 'form-group mb-2'}, [
                        m('label', {class: 'form-label'}, 'Текст'),
                        m('div', {id: 'note_body', oninput: function (e) {data.note.body = e.target.value}, value: data.note.body})
                    ]),
                    m('div', {class: 'row'},
                        m('div', {class: 'col py-2'}, [
                            m('button', {class: 'btn btn-outline-success float-end', type: 'button', onclick: note_update}, [m('i', {class: 'fa fa-save'}), ' Сохранить']),
                        ])
                    ),
                ]);
            };
            return result
        }
    }
}
