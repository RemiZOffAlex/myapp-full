function Tag() {
    let data = {
        tag: null,
        menuitem: null,
        panels: {
            standart: {
                visible: false
            }
        },
    };
    function breadcrumbs_render() {
        let result = m('ul', {class: 'breadcrumb mt-2'}, [
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/'}, m('i', {class: 'fa fa-home'}))),
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/tags'}, 'Список тегов')),
            m('li', {class: 'breadcrumb-item active'}, data.tag.name),
        ]);
        return result;
    };
    function tag_get(id) {
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": 'tag',
                "params": {
                    "id": id
                },
                "id": get_id()
            }
        }).then(
            function(response) {
                if ('result' in response) {
                    data.tag = response['result'];
                    document.title = `${data.tag.name} - ${SETTINGS.TITLE}`;
                }
            }
        );
    };
    function panels_standart_render() {
        if (data.panels.standart.visible) {
            return m('div', {class: 'row'},
                m('div', {class: 'col py-2'}, [
                    m(ComponentFavorite, {resource: data.tag, name: 'tag'}),
                    m(m.route.Link, {class: 'btn btn-outline-secondary', href: `/tag/${data.tag.id}/edit`, title: 'Редактировать'}, m('i', {class: 'fa fa-edit'})),
                ])
            );
        }
    };
    return {
        oninit: function(vnode) {
            console.log('Tag.oninit');
            tag_get(vnode.attrs.id);
        },
        view: function(vnode) {
            console.log('Tag.view');
            result = [];
            if (data.tag!=null) {
                result.push(
                    breadcrumbs_render(),
                    m('div', {class: 'row'},
                        m('div', {class: 'col h1 py-2'}, [
                            m('div', {class: 'btn-group btn-group-lg me-2'},
                                m(m.route.Link, {class: "btn btn-outline-secondary", href: "/tags", title: "Облако тегов"}, m('i', {class: 'fa fa-chevron-left'})),
                                m('button', {type: 'button', class: 'btn btn-outline-secondary', onclick: function() {panel_show(data.panels.standart)}}, m('i', {class: 'fa fa-cog'})),
                            ),
                            data.tag.name
                        ]),
                    ),
                    panels_standart_render(),
                    m('hr')
                );

                result.push({tag: MenuTag, attrs: {tag: data.tag}});

                result.push(
                    m('div', {class: 'row mt-1'},
                        m('div', {class: "col py-2"}, m.trust(data.tag.description)),
                    )
                );
                result.push(breadcrumbs_render());
            };
            return result;
        }
    };
};
