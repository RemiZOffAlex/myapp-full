function MenuTag() {
    let data = {
        menuitem: null,
        tag: null,
    };
    function button_common() {
        if (data.menuitem===null) {
            return {tag: '<', children: '<div class="btn btn-primary me-2"><i class="fa fa-bars"></i></div>'};
        } else {
            return m(m.route.Link, {class: "btn btn-outline-secondary me-2 text-decoration-none", href: `/tag/${data.tag.id}`, title: data.tag.name}, m('i', {class: 'fa fa-bars'}))
        }
    };
    function button_pages() {
        if (data.menuitem==='pages') {
            return {tag: '<', children: '<div class="btn btn-primary me-2">Статьи</div>'};
        } else {
            return m(m.route.Link, {class: "btn btn-outline-secondary me-2 text-decoration-none", href: `/tag/${data.tag.id}/pages`}, 'Статьи')
        }
    };
    function button_notes() {
        if (data.menuitem ==='notes') {
            return {tag: '<', children: '<div class="btn btn-primary me-2">Заметки</div>'};
        } else {
            return m(m.route.Link, { class: "btn btn-outline-secondary me-2 text-decoration-none", href: `/tag/${data.tag.id}/notes` }, 'Заметки')
        }
    };
    return {
        oninit: function(vnode) {
            console.log('MenuTag.oninit');
            for (let key in vnode.attrs){
                data[key] = vnode.attrs[key];
            };
        },
        onupdate: function(vnode) {
            console.log('MenuTag.onupdate');
            for (let key in vnode.attrs){
                data[key] = vnode.attrs[key];
            };
        },
        view: function() {
            if (data.tag!=null) {
                return m('div', {class: 'row'}, [
                    m('div', {class: 'col py-2'}, [
                        button_common(),
                        button_pages(),
                        button_notes(),
                    ]),
                ]);
            }
        }
    };
};
