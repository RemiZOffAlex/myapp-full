function TagEdit() {
    let data = {
        uuid: get_id(),
        tag: null,
        panels: {
            standart: {
                visible: false
            }
        },
        editor: null,
    };
    function breadcrumbs_render() {
        let result = m('ul', {class: 'breadcrumb mt-2'}, [
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/'}, m('i', {class: 'fa fa-home'}))),
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: '/tags'}, 'Список тегов')),
            m('li', {class: 'breadcrumb-item'}, m(m.route.Link, {href: `/tag/${data.tag.id}`}, data.tag.name)),
            m('li', {class: 'breadcrumb-item active'}, 'Редактирование тега'),
        ]);
        return result;
    };
    function tag_get(id) {
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": 'tag',
                "params": {
                    "id": id,
                    "fields": ["id", "name", "description"]
                },
                "id": get_id()
            }
        }).then(
            function(response) {
                if ('result' in response) {
                    data.tag = response['result'];
                }
            }
        )
    };
    function apply() {
        /* Сохранить */
        if (data.tag.name.length<2) {
            return;
        };
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": "tag.update",
                "params": {
                    "id": data.tag.id,
                    "name": data.tag.name,
                    "description": data.tag.description
                },
                "id": get_id()
            }
        }).then(
            function(response) {
                if ('result' in response) {
                }
            }
        );
    };
    function apply_and_close() {
        /* Сохранить */
        if (data.tag.name.length<2) {
            return;
        };
        m.request({
            url: '/api',
            method: "POST",
            body: {
                "jsonrpc": "2.0",
                "method": "tag.update",
                "params": {
                    "id": data.tag.id,
                    "name": data.tag.name,
                    "description": data.tag.description
                },
                "id": get_id()
            }
        }).then(
            function(response) {
                if ('result' in response) {
                    m.route.set(`/tag/${data.tag.id}`);
                }
            }
        );
    };
    function form_submit(e) {
        e.preventDefault();
        apply_and_close();
    };
    function editor_events(ed) {
        ed.on('change', function (e) {
            data.tag.description = ed.getContent();
        });
    };
    return {
        oncreate(vnode) {
            console.log('TagEdit.oncreate');
            if (data.tag!=null) {
                console.log(data.tag);
                if (data.editor==null) {
                    tinymce_config = tinymce_config_init();
                    tinymce_config.selector = `#tag_description_${data.uuid}`;
                    tinymce_config.setup = editor_events;
                    tinymce.init(tinymce_config).then(
                        function (editors) {
                            data.editor = editors[0];
                        }
                    );
                }
            }
        },
        oninit: function(vnode) {
            console.log('TagEdit.oninit');
            tag_get(Number(vnode.attrs.id));
        },
        onupdate: function(vnode) {
            console.log('TagEdit.onupdate');
            if (data.tag.id !== Number(vnode.attrs.id)) {
                tag_get(Number(vnode.attrs.id));
            };
            if (data.tag!=null) {
                if (data.editor==null) {
                    tinymce_config = tinymce_config_init();
                    tinymce_config.selector = `#tag_description_${data.uuid}`;
                    tinymce_config.setup = editor_events;
                    tinymce.init(tinymce_config).then(
                        function (editors) {
                            data.editor = editors[0];
                        }
                    );
                };
            };
        },
        view: function(vnode) {
            console.log('TagEdit.view');
            let result = [];
            if (data.tag!=null) {
                result.push(
                    breadcrumbs_render(),
                    m('div', {class: 'row'},
                        m('div', {class: 'col h1 py-2'},
                            m(m.route.Link, {class: "btn btn-outline-secondary btn-lg me-2", href: `/tag/${data.tag.id}`, title: data.tag.name}, m('i', {class: 'fa fa-chevron-left'})),
                            'Редактирование тега',
                        )
                    ),
                    m('hr'),
                    m('form', {onsubmit: form_submit}, [
                        m('div', {class: 'mb-2'}, [
                            m('label', {class: 'form-label'}, 'Заголовок'),
                            m('input', {class: 'form-control', type: 'text', oninput: function (e) {data.tag.name = e.target.value}, value: data.tag.name}, 'Заголовок'),
                        ]),
                        m('div', {class: 'mb-2'}, [
                            m('label', {class: 'form-label'}, 'Текст'),
                            m('textarea', { class: 'form-control', cols: '40', rows: '8', id: `tag_description_${data.uuid}`, oninput: function (e) {data.tag.description = e.target.value}, value: data.tag.description})
                        ]),
                        m('div', {class: 'row'},
                            m('div', {class: 'col py-2 text-end'}, [
                                m('button', {class: 'btn btn-outline-success btn-lg me-2', type: 'button', onclick: apply}, [m('i', {class: 'fa fa-save me-2'}), 'Сохранить']),
                                m('button', {class: 'btn btn-outline-success btn-lg', type: 'submit'}, [m('i', {class: 'fa fa-save me-2'}), 'Сохранить и закрыть']),
                            ])
                        ),
                    ]),
                    breadcrumbs_render()
                );
            };
            return result;
        }
    };
};
