const SETTINGS = {
    ITEMS_ON_PAGE: {{ ITEMS_ON_PAGE }},
    TITLE: 'Моя панель',
    ME: {{ user|tojson|safe }},
    STATIC: '{{ STATIC }}',
}
