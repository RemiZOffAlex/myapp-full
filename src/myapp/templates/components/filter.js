function PanelFilter() {
    let data = {
        visible: false,
        value: '',
        isregex: false,
    };
    function filter_clear() {
        data.value = '';
    };
    function filter_apply() {};
    function form_submit(e) {
        e.preventDefault();
        filter_apply();
    };
    function button_isregex_render() {
        if (data.isregex) {
            return m('button', {class: 'btn btn-outline-secondary', type: 'button', onclick: function() { data.isregex = false;}}, '(.*)');
        } else {
            return m('button', {class: 'btn btn-outline-secondary', type: 'button', onclick: function() { data.isregex = true;}}, 'T');
        }
    };
    return {
        data: data,
        oninit: function(vnode) {
            console.log('PanelFilter.oninit');
            for (let key in vnode.attrs){
                data[key] = vnode.attrs[key];
            };
        },
        view: function() {
            console.log('PanelFilter.view');
            result = [];
            if (data.visible){
                result.push(
                    m('div', {class: "row"},
                        m('div', {class: "col py-2"},
                            m('form', {onsubmit: form_submit},
                                m('div', {class: "input-group mb-3"}, [
                                    m('button', {class: 'btn btn-outline-danger', onclick: function() {filter_clear()}}, m('i', {class: 'fa fa-remove'})),
                                    button_isregex_render(),
                                    m('input', {class: 'form-control', oninput: function (e) {data.value = e.target.value}, value: data.value}),
                                    m('button', {class: 'btn btn-outline-success', onclick: function() {filter_apply()}}, m('i', {class: 'fa fa-check'})),
                                ])
                            )
                        )
                    )
                );
            };
            return result;
        }
    }
};
