function Backtotop() {
    let data = {
        get visible() {
            return data.raw_visible;
        },
        set visible(value) {
            if (data.raw_visible!=value) {
                m.redraw();
            }
            data.raw_visible = value;
        },
        raw_visible: false,
    }
    function backToTop() {
        let currentScroll = document.documentElement.scrollTop || document.body.scrollTop
        if (currentScroll > 0) {
            window.scrollTo(0, 0)
        }
    };
    function catchScroll() {
        data.visible = (window.pageYOffset > 100);
    };
    return {
        oninit: function(vnode) {
            window.addEventListener('scroll', catchScroll);
            let currentScroll = document.documentElement.scrollTop || document.body.scrollTop
            data.visible = (currentScroll > 100);
        },
        onremove: function(vnode) {
            window.removeEventListener('scroll', catchScroll)
        },
        view: function(vnode) {
            if (data.visible) {
                return m('div', {class: 'scrollToTop', style: 'z-index: 10;', onclick: backToTop},
                    m('div', {class: 'card'},
                        m('div', {class: 'card-body py-2 px-2'},
                            m('i', {class: 'fa fa-chevron-up'})
                        )
                    )
                );
            }
        }
    };
};
