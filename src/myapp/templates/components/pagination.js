/*
pagination: Pagination({
    clickHandler: resumes_get,
    prefix_url: '/resumes'
}),

oninit: function(vnode) {
    let pagination = data.pagination.data;
    if (vnode.attrs.page!==undefined) {
        pagination.page = Number(vnode.attrs.page);
    }
    resumes_get();
},
*/
function Pagination(arguments) {
    let data = {
        page: 1,
        per_page: SETTINGS.ITEMS_ON_PAGE,
        size: 0,
        prefix_url: '',
        clickHandler: function() {}
    };
    for (let key in arguments){
        data[key] = arguments[key];
    };
    function pages() {
        return Math.ceil(data.size/data.per_page);
    };
    function handlePageSelected(selected) {
        data.page = selected;
        m.route.set(`${data.prefix_url}/${selected}`)
        data.clickHandler();
    };
    function has_prev() {
        return data.page > 1;
    };
    function has_next() {
        return data.page < pages();
    };
    function iter_pages() {
        /* */
        let last = 0;
        let left_edge=2, left_current=2,
                right_current=5, right_edge=2;
        let result = [];
        for (let num = 1; num < pages()+1; num++) {
            if (num <= left_edge ||
                (num > data.page - left_current - 1 &&
                num < data.page + right_current) ||
                num > pages() - right_edge) {
                if (last + 1 != num) {
                    result.push(null);
                } else {
                    result.push(num);
                }
                last = num
            }
        };
        return result;
    };
    return {
        data: data,
        view: function(vnode) {
            console.log('Pagination.view');
            let result = [
                m('div', {class: "row"},
                    m('div', {class: "col py-2 text-center"}, 
                        (function() {
                            if (pages()<=1) {
                                return m('button', {class: "btn btn-outline-secondary", type: "button", onclick: function() { data.clickHandler(1)}}, m('i', {class: "fa fa-refresh"}));
                            } else {
                                return [
                                    m('div', {class: "d-none d-lg-block"},
                                        (function() {
                                            let result = [];
                                            if (has_prev()) {
                                                result.push(
                                                    m('button', {class: "btn btn-outline-secondary pull-left", type: "button", onclick: function() { handlePageSelected(data.page-1)}}, 'Предыдущая')
                                                );
                                            }
                                            let buttons = iter_pages().map( 
                                                function(item) {
                                                    if (item!==null) {
                                                        return m('button', {class: "btn btn-outline-secondary me-1", type: 'button', onclick: function() { handlePageSelected(item)} }, item);
                                                    } else {
                                                        return m('button', {class: "btn btn-outline-secondary me-1", type: 'button', onclick: function() { handlePageSelected(data.page)} }, m('i', {class: "fa fa-refresh"}));
                                                    }
                                                }
                                            );
                                            result.push(...buttons);
                                            if (has_next()) {
                                                result.push(
                                                    m('button', {type: "button", class: "btn btn-outline-secondary float-end", onclick: function() { handlePageSelected(data.page+1)}}, 'Следующая')
                                                );
                                            }
                                            return result;
                                        })()
                                    ),
                                    m('div', {class: "d-lg-none"},
                                        m('div', {class: "btn-group w-100"},
                                            (function() {
                                                let result = [];
                                                if (has_prev()) {
                                                    result.push(
                                                        m('button', {class: "btn btn-outline-secondary pull-left", type: "button", onclick: function() { handlePageSelected(data.page-1)}}, m('i', {class: "fa fa-chevron-left"}))
                                                    );
                                                }
                                                result.push(
                                                    m('button', {class: "btn btn-outline-secondary w-100", type: "button"}, data.page)
                                                );
                                                if (has_next()) {
                                                    result.push(
                                                        m('button', {class: "btn btn-outline-secondary float-end", type: "button", onclick: function() { handlePageSelected(data.page+1)}}, m('i', {class: "fa fa-chevron-right"}))
                                                    )
                                                }
                                                return result;
                                            })()
                                        )
                                    )
                                ]
                            }
                        })()
                    )
                )
            ];
            return result;
        }
    }
};
/*
<div class="row">
<div class="col py-2 text-center">
<button type="button" class="btn btn-outline-secondary" v-if="pages()<=1" v-on:click="refresh"><i class="fa fa-refresh"></i></button>

<template v-else>
<div class="d-none d-lg-block">

<button type="button" class="btn btn-outline-secondary pull-left" v-if="has_prev" v-on:click="handlePageSelected(pagination.page-1)">Предыдущая</button>

<template v-for="page in iter_pages">
<button type="button" class="btn btn-outline-secondary me-1" v-if="page" v-on:click="handlePageSelected(page)">{# page #}</button>
<button type="button" class="btn btn-outline-secondary me-1" v-else v-on:click="refresh"><i class="fa fa-refresh"></i></button>
</template>

<button type="button" class="btn btn-outline-secondary float-end" v-if="has_next" v-on:click="handlePageSelected(pagination.page+1)">Следующая</button>

</div>
<div class="d-lg-none">
<div class="btn-group w-100">
<button type="button" class="btn btn-outline-secondary" v-if="has_prev" v-on:click="handlePageSelected(pagination.page-1)"><i class="fa fa-chevron-left"></i></button>
<button type="button" class="btn btn-outline-secondary w-100">{# pagination.page #}</button>
<button type="button" class="btn btn-outline-secondary" v-if="has_next" v-on:click="handlePageSelected(pagination.page+1)"><i class="fa fa-chevron-right"></i></button>
</div>
</div>
</template>

</div>
</div>
*/
