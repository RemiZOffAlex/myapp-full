function ComponentInfo() {
    let data = {
        item: null,
    }
    return {
        oninit: function(vnode) {
            console.log('ComponentInfo.oninit');
            for (let key in vnode.attrs){
                data[key] = vnode.attrs[key];
            };
        },
        onbeforeupdate: function(vnode) {
            console.log('ComponentInfo.onbeforeupdate');
            for (let key in vnode.attrs){
                data[key] = vnode.attrs[key];
            };
        },
        view: function(vnode) {
            console.log('ComponentInfo.view');
            let result = [];
            if (data.item!=null) {
                result.push(
                    m('div', {class: 'row'},
                        m('div', {class: 'col text-muted py-2'},
                            m('small', [
                                m('i', {class: 'fa fa-user me-1'}),
                                m(m.route.Link, {class: 'me-2', href: `/user/${data.item.user.id}`}, data.item.user.name),
                                {tag: "<", children: `Создано: ${data.item.created}`},
                                {tag: "<", children: '&ensp;'},
                                {tag: "<", children: `Обновлено: ${data.item.updated}`},
                            ])
                        )
                    )
                );
            };
            return result;
        }
    }
};
