function PanelOrderBy(arguments) {
    let data = {
        visible: false,
        field: 'id',
        order: 'desc',
        fields: [{value: 'id', text: 'ID'}],
        clickHandler: function() {}
    }
    for (let key in arguments){
        data[key] = arguments[key];
    }
    return {
        data: data,
        value: function() {
            return {
                "field": data.field,
                "order": data.order
            };
        },
        oninit: function(vnode) {
            console.log('PanelOrderBy.oninit');
            for (let key in vnode.attrs){
                data[key] = vnode.attrs[key];
            };
        },
        view: function(vnode) {
            console.log('PanelOrderBy.view');
            if (data.visible) {
                let options = data.fields.map(
                    function(item) {
                        return m('option', {value: item.value}, item.text);
                    }
                );
                let order_button = null; 
                if (data.order==='asc') {
                    order_button = m('button', {type: "button", class: "btn btn-outline-secondary", onclick: function() { data.order = 'desc'; data.clickHandler() }}, 
                        m('i', {class: "fa fa-sort-alpha-asc"})
                    );
                } else {
                    order_button = m('button', {type: "button", class: "btn btn-outline-secondary", onclick: function() { data.order = 'asc'; data.clickHandler() }}, 
                        m('i', {class: "fa fa-sort-alpha-desc"})
                    );
                }
                return [
                    m('div', {class: "row"}, [
                        m('div', {class: "col py-2"}, [
                            m('div', {class: "input-group"}, [
                                order_button,
                                m('select', {class: "form-select", onchange: function(e) { data.field = e.target.value; data.clickHandler() }}, [...options])
                            ])
                        ])
                    ])
                ];
            };
        }
    }
};
