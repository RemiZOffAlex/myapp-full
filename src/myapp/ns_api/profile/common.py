__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

from .. import jsonrpc, login_required
from ... import lib
from ...mutations.user import user_as_dict


@jsonrpc.method('profile')
@login_required
def profile(
    fields: list = ['id', 'name']
) -> dict:
    """Показать список пользователей
    """
    user = lib.get_user()

    result = user_as_dict(user, fields)
    return result
