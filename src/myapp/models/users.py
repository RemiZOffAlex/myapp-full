__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import datetime
from sqlalchemy import (
    Column,
    Boolean,
    Integer,
    String,
    DateTime
)
from sqlalchemy.orm import relationship

from . import Base


class User(Base):
    """Пользователи
    """
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    password = Column(String, nullable=False)
    disabled = Column(Boolean, default=True)
    created = Column(DateTime)

    # Связи
    pages = relationship("Page", primaryjoin="Page.user_id==User.id")

    def __init__(self, name):
        self.name = name
        self.created = datetime.datetime.utcnow()

    def as_dict(self):
        return {
            c.name: getattr(self, c.name)
            for c in self.__table__.columns
            if c.name != 'password'
        }
