"""
Таблицы:
    Tag
    TagLogo
"""

__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from . import Base


class Tag(Base):
    """Теги, метки
    """
    __tablename__ = "tag"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, unique=True)

    # Связи
    pages = relationship("PageTag", primaryjoin="Tag.id==PageTag.tag_id")

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "<Tag('%s')>" % (self.name)

    def as_dict(self):
        """Возвращает словарь
        """
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
