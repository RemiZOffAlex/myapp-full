__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_utils import database_exists, create_database

from .. import app

engine = create_engine(
    app.config['SQLALCHEMY_DATABASE_URI'],
    echo=app.config['SQLDEBUG']
)
if not database_exists(engine.url):
    create_database(engine.url)

db_session = scoped_session(
    sessionmaker(
        autocommit=False,
        autoflush=False,
        bind=engine
    )
)
Base = declarative_base()
Base.query = db_session.query_property()

# Пользователи
from .users import User # noqa F401

# Метки
from .tag import Tag # noqa F401

# Заметки
from .note.common import Note # noqa F401
from .note.favorite import NoteFavorite # noqa F401
from .note.tag import NoteTag # noqa F401
from .note.trash import NoteTrash # noqa F401
from .note.tree import NoteTree # noqa F401

# Статьи
from .page.common import Page # noqa F401
from .page.favorite import PageFavorite # noqa F401
from .page.tag import PageTag # noqa F401
from .page.trash import PageTrash # noqa F401
from .page.tree import PageTree # noqa F401

Base.metadata.create_all(engine)

__all__ = []
