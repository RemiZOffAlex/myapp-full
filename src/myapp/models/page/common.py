__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import datetime
from sqlalchemy import Column, Integer, ForeignKey, String, DateTime
from sqlalchemy.orm import relationship

from .. import Base


class Page(Base):
    """Страницы
    """
    __tablename__ = "page"

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    title = Column(String)
    body = Column(String, default='')
    created = Column(DateTime)  # Дата создания
    updated = Column(DateTime)  # Дата обновления

    # Связи
    user = relationship(
        "User",
        primaryjoin="Page.user_id==User.id",
        uselist=False
    )
    tags = relationship("PageTag", primaryjoin="Page.id==PageTag.page_id")
    # Родитель
    parents = relationship(
        "PageTree",
        primaryjoin="Page.id == PageTree.child_id",
    )
    # Дочерние узлы
    nodes = relationship(
        "PageTree",
        primaryjoin="Page.id == PageTree.parent_id",
    )
    trash = relationship(
        "PageTrash",
        primaryjoin="Page.id == PageTrash.page_id",
        uselist=False
    )

    def __init__(self, user, title):
        assert type(user).__name__ == 'User', 'Не передан объект User'
        self.user_id = user.id
        self.title = title
        self.created = datetime.datetime.now()
        self.updated = datetime.datetime.now()

    def __repr__(self):
        return "<Page('{}')>".format(self.title)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
