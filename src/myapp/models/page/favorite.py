__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import datetime
from sqlalchemy import Column, Boolean, Integer, ForeignKey, String, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.schema import Sequence

from .. import Base


class PageFavorite(Base):
    """Избранные статьи
    """
    __tablename__ = "page_favorite"

    # ID
    id = Column(Integer, primary_key=True, index=True, unique=True)
    user_id = Column(Integer, ForeignKey('user.id'), index=True)
    page_id = Column(Integer, ForeignKey('page.id'), index=True)
    created = Column(DateTime)

    # Связи
    page = relationship(
        "Page",
        primaryjoin="PageFavorite.page_id == Page.id",
        uselist=False
    )
    user = relationship(
        "User",
        primaryjoin="PageFavorite.user_id == User.id",
        uselist=False
    )

    def __init__(self, user, page):
        assert type(user).__name__ == 'User', 'Не передан объект User'
        assert type(page).__name__ == 'Page', 'Не передан объект Page'
        self.user_id = user.id
        self.page_id = page.id
        self.created = datetime.datetime.now()

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
