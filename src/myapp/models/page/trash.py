__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import datetime
from sqlalchemy import Column, Integer, ForeignKey, DateTime, String
from sqlalchemy.orm import relationship

from .. import Base


class PageTrash(Base):
    """Корзина для страниц справки
    """
    __tablename__ = "page_trash"

    id = Column(Integer, primary_key=True, index=True)
    # ID пользователя
    user_id = Column(Integer, ForeignKey('user.id'), index=True)
    # ID страницы
    page_id = Column(Integer, ForeignKey('page.id'), index=True)
    # Дата удаления
    created = Column(DateTime)

    # Связи
    user = relationship(
        "User",
        primaryjoin="PageTrash.user_id == User.id",
        uselist=False
    )
    page = relationship(
        "Page",
        primaryjoin="PageTrash.page_id == Page.id",
        back_populates="trash",
        uselist=False
    )

    def __init__(self, user, page):
        assert type(user).__name__ == 'User', 'Не передан объект User'
        assert type(page).__name__ == 'Page', 'Не передан объект Page'
        self.user_id = user.id
        self.page_id = page.id
        self.created = datetime.datetime.now()
