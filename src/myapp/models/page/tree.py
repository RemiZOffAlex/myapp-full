__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import datetime
from sqlalchemy import Table, Column, Boolean, Integer, ForeignKey, String, DateTime
from sqlalchemy.orm import relationship

from .. import Base


class PageTree(Base):
    """Справка по сервису
    """
    __tablename__ = "page_tree"

    id = Column(Integer, primary_key=True, index=True)
    parent_id = Column(Integer, ForeignKey('page.id'), index=True) # ссылку на предка (ancestor)
    child_id = Column(Integer, ForeignKey('page.id'), index=True) # ссылку на потомка (descendant)
    level = Column(Integer, default=0) # Уровень относительно родителя

    # Связи
    # Родитель
    parent = relationship(
        "Page",
        primaryjoin="Page.id == PageTree.parent_id",
        uselist=False
    )
    # Дочерний узел
    child = relationship(
        "Page",
        primaryjoin="Page.id == PageTree.child_id",
        uselist=False
    )

    def __init__(self, parent, child):
        assert type(parent).__name__ == 'Page', 'Не передан объект Page'
        assert type(child).__name__ == 'Page', 'Не передан объект Page'
        self.parent_id = parent.id
        self.child_id = child.id

    def __repr__(self):
        return "<PageTree({id}, '{parent}', '{child}')>".format(
            id=self.id,
            parent=self.parent,
            child=self.child
        )

    def __json__(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
