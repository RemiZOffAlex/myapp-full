__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import datetime
from sqlalchemy import Table, Column, Boolean, Integer, ForeignKey, String, DateTime
from sqlalchemy.orm import relationship

from .. import Base


class NoteTree(Base):
    """Справка по сервису
    """
    __tablename__ = "note_tree"

    id = Column(Integer, primary_key=True, index=True)
    parent_id = Column(Integer, ForeignKey('note.id'), index=True) # ссылку на предка (ancestor)
    child_id = Column(Integer, ForeignKey('note.id'), index=True) # ссылку на потомка (descendant)
    level = Column(Integer, default=0) # Уровень относительно родителя

    # Связи
    # Родитель
    parent = relationship(
        "Note",
        primaryjoin="Note.id == NoteTree.parent_id",
        uselist=False
    )
    # Дочерний узел
    child = relationship(
        "Note",
        primaryjoin="Note.id == NoteTree.child_id",
        uselist=False
    )

    def __init__(self, parent, child):
        assert type(parent).__name__ == 'Note', 'Не передан объект Note'
        assert type(child).__name__ == 'Note', 'Не передан объект Note'
        self.parent_id = parent.id
        self.child_id = child.id

    def __repr__(self):
        return "<NoteTree({id}, '{parent}', '{child}')>".format(
            id=self.id,
            parent=self.parent,
            child=self.child
        )

    def __json__(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
