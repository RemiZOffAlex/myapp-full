__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import datetime
from sqlalchemy import Column, Integer, ForeignKey, String, DateTime
from sqlalchemy.orm import relationship

from .. import Base


class NoteTag(Base):
    """Теги к заметкам"""
    __tablename__ = "note_tag"

    id = Column(Integer, primary_key=True)
    note_id = Column(Integer, ForeignKey('note.id'))
    tag_id = Column(Integer, ForeignKey('tag.id'))

    # Связи
    note = relationship(
        "Note",
        primaryjoin="NoteTag.note_id==Note.id",
        uselist=False
    )
    tag = relationship(
        "Tag",
        primaryjoin="NoteTag.tag_id==Tag.id",
        uselist=False
    )

    def __init__(self, note, tag):
        assert type(note).__name__ == 'Note', 'Не передан объект Note'
        assert type(tag).__name__ == 'Tag', 'Не передан объект Tag'
        self.note_id = note.id
        self.tag_id = tag.id

    def __repr__(self):
        return "<NoteTag('%s')>" % (self.id)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
