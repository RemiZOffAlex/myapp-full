__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import datetime
from sqlalchemy import Column, Boolean, Integer, ForeignKey, String, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.schema import Sequence

from .. import Base


class NoteFavorite(Base):
    """Избранные статьи
    """
    __tablename__ = "note_favorite"

    # ID
    id = Column(Integer, primary_key=True, index=True, unique=True)
    user_id = Column(Integer, ForeignKey('user.id'), index=True)
    note_id = Column(Integer, ForeignKey('note.id'), index=True)
    created = Column(DateTime)

    # Связи
    note = relationship(
        "Note",
        primaryjoin="NoteFavorite.note_id == Note.id",
        uselist=False
    )
    user = relationship(
        "User",
        primaryjoin="NoteFavorite.user_id == User.id",
        uselist=False
    )

    def __init__(self, user, note):
        assert type(user).__name__ == 'User', 'Не передан объект User'
        assert type(note).__name__ == 'Note', 'Не передан объект Note'
        self.user_id = user.id
        self.note_id = note.id
        self.created = datetime.datetime.now()

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
