__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import datetime
from sqlalchemy import Column, Integer, ForeignKey, DateTime, String
from sqlalchemy.orm import relationship

from .. import Base


class NoteTrash(Base):
    """Корзина для страниц справки
    """
    __tablename__ = "note_trash"

    id = Column(Integer, primary_key=True, index=True)
    # ID пользователя
    user_id = Column(Integer, ForeignKey('user.id'), index=True)
    # ID страницы
    note_id = Column(Integer, ForeignKey('note.id'), index=True)
    # Дата удаления
    created = Column(DateTime)

    # Связи
    user = relationship(
        "User",
        primaryjoin="NoteTrash.user_id == User.id",
        uselist=False
    )
    note = relationship(
        "Note",
        primaryjoin="NoteTrash.note_id == Note.id",
        back_populates="trash",
        uselist=False
    )

    def __init__(self, user, note):
        assert type(user).__name__ == 'User', 'Не передан объект User'
        assert type(note).__name__ == 'Note', 'Не передан объект Note'
        self.user_id = user.id
        self.note_id = note.id
        self.created = datetime.datetime.now()
