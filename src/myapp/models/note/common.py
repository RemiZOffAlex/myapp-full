__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import datetime
from sqlalchemy import Column, Integer, ForeignKey, String, DateTime
from sqlalchemy.orm import relationship

from .. import Base


class Note(Base):
    """Заметки
    """
    __tablename__ = "note"

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    title = Column(String)
    body = Column(String, default='')
    created = Column(DateTime)  # Дата создания
    updated = Column(DateTime)  # Дата обновления

    # Связи
    user = relationship(
        "User",
        primaryjoin="Note.user_id==User.id",
        uselist=False
    )
    tags = relationship("NoteTag", primaryjoin="Note.id==NoteTag.note_id")
    # Родитель
    parents = relationship(
        "NoteTree",
        primaryjoin="Note.id == NoteTree.child_id",
    )
    # Дочерние узлы
    nodes = relationship(
        "NoteTree",
        primaryjoin="Note.id == NoteTree.parent_id",
    )
    trash = relationship(
        "NoteTrash",
        primaryjoin="Note.id == NoteTrash.note_id",
        uselist=False
    )

    def __init__(self, user, title):
        assert type(user).__name__ == 'User', 'Не передан объект User'
        self.user_id = user.id
        self.title = title
        self.created = datetime.datetime.now()
        self.updated = datetime.datetime.now()

    def __repr__(self):
        return "<Note({}, '{}')>".format(self.id, self.title)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
