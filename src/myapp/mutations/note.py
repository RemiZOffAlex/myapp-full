__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import logging

from .. import lib, models


log = logging.getLogger(__name__)


def note_as_dict(
    note: models.Note,
    fields: list = ['id', 'title']
):
    """Статью как словарь (в JSON)
    """
    def field_favorite(note):
        # Избранное
        assert lib.get_user(), 'favorite only authorized users'
        result = False
        favorite = models.db_session.query(
            models.NoteFavorite
        ).filter(
            models.NoteFavorite.note_id == note.id,
            models.NoteFavorite.user_id == lib.get_user().id
        ).first()
        if favorite:
            result = True
        return result

    def field_tags(note):
        # Теги
        result = []
        for tagLink in note.tags:
            newTag = tagLink.tag.as_dict()
            result.append(newTag)
        return result

    def field_user(note):
        # Пользователь
        return note.user.as_dict()

    def field_parents(note):
        # Родители
        result = []
        parent = note.parent
        while parent:
            result.append(parent.as_dict())
            parent = parent.parent
        result.reverse()
        return result

    def field_nodes(note):
        # Дети
        result = []
        for item in note.nodes:
            result.append(note_as_dict(item))
        return result

    funcs = {
        'favorite': field_favorite,
        'tags': field_tags,
        'user': field_user,
        'parents': field_parents,
        'nodes': field_nodes
    }
    result = {}

    for column in note.__table__.columns:
        if column.name in fields:
            result[column.name] = getattr(note, column.name)

    for field in fields:
        if field in funcs:
            func = funcs[field]
            result[field] = func(note)

    log.info(result)
    return result
