__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import logging

from .. import lib, models


log = logging.getLogger(__name__)


def user_as_dict(
    user: models.User,
    fields: list = ['id', 'name']
):
    """Пользователя как словарь (в JSON)
    """
    def field_favorite(user):
        # Избранное
        assert lib.get_user(), 'favorite only authorized users'
        result = False
        favorite = models.db_session.query(
            models.PageFavorite
        ).filter(
            models.PageFavorite.user_id == user.id,
            models.PageFavorite.user_id == lib.get_user().id
        ).first()
        if favorite:
            result = True
        return result

    def field_tags(user):
        # Теги
        result = []
        for tagLink in user.tags:
            newTag = tagLink.tag.as_dict()
            result.append(newTag)
        return result

    funcs = {
        'favorite': field_favorite,
        'tags': field_tags
    }
    result = {}

    for column in user.__table__.columns:
        if column.name in fields:
            if column.name not in ['password']:
                result[column.name] = getattr(user, column.name)

    for field in fields:
        if field in funcs:
            func = funcs[field]
            result[field] = func(user)

    log.info(result)
    return result
