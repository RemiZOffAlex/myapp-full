__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import logging

from .. import models


log = logging.getLogger(__name__)


def tag_as_dict(
    tag: models.Tag,
    fields: list = ['id', 'name']
):
    """Тег в словарь
    """
    def field_pages(tag):
        # Теги
        result = []
        for tagLink in tag.tags:
            newTag = tagLink.tag.as_dict()
            result.append(newTag)
        return result

    def field_user(tag):
        # Пользователь
        return tag.user.as_dict()

    funcs = {
        'pages': field_pages,
        'user': field_user,
    }
    result = {}

    for column in tag.__table__.columns:
        if column.name in fields:
            result[column.name] = getattr(tag, column.name)

    for field in fields:
        if field in funcs:
            func = funcs[field]
            result[field] = func(tag)

    return result
