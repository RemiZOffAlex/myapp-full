__author__ = 'RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

import logging

from .. import lib, models


log = logging.getLogger(__name__)


def page_as_dict(
    page: models.Page,
    fields: list = ['id', 'title']
):
    """Статью как словарь (в JSON)
    """
    def field_favorite(page):
        # Избранное
        assert lib.get_user(), 'favorite only authorized users'
        result = False
        favorite = models.db_session.query(
            models.PageFavorite
        ).filter(
            models.PageFavorite.page_id == page.id,
            models.PageFavorite.user_id == lib.get_user().id
        ).first()
        if favorite:
            result = True
        return result

    def field_tags(page):
        # Теги
        result = []
        for tagLink in page.tags:
            newTag = tagLink.tag.as_dict()
            result.append(newTag)
        return result

    def field_user(page):
        # Пользователь
        return page.user.as_dict()

    def field_parents(page):
        # Родители
        result = []
        parent = page.parent
        while parent:
            result.append(parent.as_dict())
            parent = parent.parent
        result.reverse()
        return result

    def field_nodes(page):
        # Дети
        result = []
        for item in page.nodes:
            result.append(page_as_dict(item))
        return result

    funcs = {
        'favorite': field_favorite,
        'tags': field_tags,
        'user': field_user,
        'parents': field_parents,
        'nodes': field_nodes
    }
    result = {}

    for column in page.__table__.columns:
        if column.name in fields:
            result[column.name] = getattr(page, column.name)

    for field in fields:
        if field in funcs:
            func = funcs[field]
            result[field] = func(page)

    log.info(result)
    return result
