__author__ = 'RemiZOffAlex'
__copyright__ = '(c) RemiZOffAlex'
__email__ = 'remizoffalex@mail.ru'

from flask import render_template, Response

from myapp import app, lib


@app.route('/app.js')
def app_js():
    """Фильтр
    """
    pagedata = {}
    if lib.get_user():
        body = render_template('/private/app.js', pagedata=pagedata)
    else:
        body = render_template('/public/app.js', pagedata=pagedata)
    return Response(body, mimetype='application/javascript')


@app.route("/robots.txt")
def robots_txt():
    body = render_template("robots.txt")
    return Response(body, mimetype='text/plain')


# noinspection PyUnusedLocal
@app.errorhandler(404)
def error_missing(exception):
    pagedata = {}
    error_message = "Не судьба..."
    return render_template(
        "error.html",
        error_code=404,
        error_message=error_message,
        pagedata=pagedata
    ), 404


# noinspection PyUnusedLocal
@app.errorhandler(403)
def error_unauthorized(exception):
    pagedata = {}
    error_message = "У вас нет прав"
    return render_template(
        "error.html",
        error_code=403,
        error_message=error_message,
        pagedata=pagedata
    ), 403


# noinspection PyUnusedLocal
@app.errorhandler(500)
def error_crash(exception):
    pagedata = {}
    error_message = "Вот незадача..."
    return render_template(
        "error.html",
        error_code=500,
        error_message=error_message,
        pagedata=pagedata
    ), 500
