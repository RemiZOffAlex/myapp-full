Flask
git+https://gitlab.com/RemiZOffAlex/jsonrpc@master#egg=jsonrpc
alembic
SQLAlchemy
SQLAlchemy-Utils
flake8
gunicorn
psycopg2-binary
