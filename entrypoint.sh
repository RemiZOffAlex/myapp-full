#!/bin/sh

alembic --config .alembic.ini upgrade head
gunicorn -w 4 --bind=0.0.0.0 myapp:app
