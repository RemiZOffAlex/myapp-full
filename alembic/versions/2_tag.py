"""Tag

Revision ID: 61ede83f5cd4
Revises: 1d212513b25f
Create Date: 2018-02-25 14:10:08.770210

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '61ede83f5cd4'
down_revision = '1d212513b25f'
branch_labels = None
depends_on = None


def upgrade():
    # Объекты доступа: процедура и содержащий процедуру модуль
    op.create_table('tag',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False, unique=True),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table("tag")
