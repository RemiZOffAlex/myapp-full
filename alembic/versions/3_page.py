"""empty message

Revision ID: da6bc5b8d253
Revises: 61ede83f5cd4
Create Date: 2020-08-19 04:51:11.868138

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'da6bc5b8d253'
down_revision = '61ede83f5cd4'
branch_labels = None
depends_on = None


def upgrade():
    # Статьи
    op.create_table('page',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('user_id', sa.Integer(), nullable=True),
        sa.Column('title', sa.String(), nullable=True),
        sa.Column('body', sa.String(), nullable=True),
        sa.Column('created', sa.DateTime(), nullable=True),
        sa.Column('updated', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
        sa.PrimaryKeyConstraint('id')
    )

    # Теги к заметкам
    op.create_table('tagpage',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('page_id', sa.Integer(), nullable=True),
        sa.Column('tag_id', sa.Integer(), nullable=True),
        sa.Column('created', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['page_id'], ['page.id'], ),
        sa.ForeignKeyConstraint(['tag_id'], ['tag.id'], ),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table("tagpage")
    op.drop_table("page")
